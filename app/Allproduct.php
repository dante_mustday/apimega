<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Allproduct extends Model
{
    protected $table = 'allproduct_ec';
	
	public function category(){
        return $this->hasMany(Category::class,'category_id','id_cat');
    }

}
