<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Banner extends Model
{
    protected $table = 'banner_ec';

    public function actived(){
        return $this->where('removed_banner',0)->where('status_banner',1);
    }
}
