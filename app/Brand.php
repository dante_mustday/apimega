<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Brand extends Model
{
    protected $table = 'brand_ec';

    public function actived(){
        return $this->where('removed_brand',0)->where('status_brand',1);
    }
}
