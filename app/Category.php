<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $table = 'category_ec';

    public function actived(){
        return $this->where('removed_cat',0)->where('status_cat',1);
    }
	
	public function subcategory(){
        return $this->hasMany(Subcategory::class,'parent_scat','id_cat')->where('removed_scat',0)->where('status_scat',1);
    }
	
	public function product(){
        return $this->hasMany(Allproduct::class,'category_id','id_cat')->orderBy('seq_var','ASC')->take(6);
    }
	
	public function relatedinventory(){
        return $this->hasMany(Categoryinventory::class,'cat_id','id_cat');
    }
}
