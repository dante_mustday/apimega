<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

class Categoryinventory extends Model
{
    protected $table = 'category_selected_inventory';
	
	public function category(){
        return $this->belongsTo(Category::class,'cat_id','id_cat');
    }
	
	public function product(){
        return $this->hasOne(Allproduct::class,'id_var','inv_id');
    }

}