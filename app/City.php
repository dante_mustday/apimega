<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    protected $table = 'kabupaten_ec';

    public function actived(){
        return $this->where('removed_kabupaten',0)->where('status_kabupaten',1);
    }
}
