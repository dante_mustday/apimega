<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discount extends Model
{
    protected $table = 'discount_ec';
	
	public function details(){
        return $this->hasMany(Discountdetail::class,'disc_dd','id_disc');
    }
}
