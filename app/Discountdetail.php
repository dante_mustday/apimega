<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Discountdetail extends Model
{
    protected $table = 'discount_detail_ec';
	
	public function variant(){
        return $this->hasMany(Variants::class,'var_dd','id_var');
    }
	
	public function parents(){
		$now = date("Y-m-d h:i:s");
        return $this->belongsTo(Discount::class,'disc_dd','id_disc')
		->where('start_disc','>=',$now)
		->where('end_disc','<=',$now)
		->where('status_disc', '1')
		->where('removed_disc',0);
    }
}
