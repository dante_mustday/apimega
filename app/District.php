<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    protected $table = 'ongkir_jne_ec';

    public function actived(){
        return $this->where('removed_shipping',0)->where('status_shipping',1);
    }
}
