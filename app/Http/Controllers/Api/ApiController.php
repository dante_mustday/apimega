<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\ValidationException;
use DB;
class ApiController extends Controller
{
    /**
     * Send the error response.
     *
     * @param  string|\Exception $message
     * @param  int $httpCode
     * @return \Illuminate\Http\Response
     */
	 
	public $data = [];
    public function __construct() {
        DB::enableQueryLog(); 
		
		$start = 0;
		$limit = 12;
		$x='';
		if(isset($_GET['limit'])){
			$limit = $_GET['limit'];
		}
		if(isset($_GET['start'])){
			$start = $_GET['start'];
		}
		$this->limit = $limit;
		$this->start = $start;
	}
	 
    protected function error($message, $httpCode = 500, $isValidationMessage = false)
    {
        logger($message);
        if(config('app.debug') || $isValidationMessage) {
            if ($message instanceof \Exception || $message instanceof \InvalidArgumentException) {
                $message = $message->getMessage();
                //app('sentry')->captureException($message);
            }
        } else {
            $message = 'Something error with your request. Please contact your administrator';
        }

        return response()->json(
            $this->generateMessage('Error', $message),
            $httpCode
        );
    }

    public function empty($message = null)
    {
        $message = null === $message ? 'Empty Response.' : $message;

        return response()->json(
            $this->generateMessage('Empty', $message), 200
        );
    }

    /**
     * Send the not found response.
     *
     * @return \Illuminate\Http\Response
     */
    protected function notFound($message = null)
    {
        $message = null === $message ? 'Not Found.' : $message;

        return response()->json(
            $this->generateMessage('Error', $message), 404
        );
    }

    /**
     * Send the success response.
     *
     * @param  mixed|null $message
     * @param  integer $httpCode
     * @return \Illuminate\Http\Response
     */
    protected function success($message = null, $httpCode = 200)
    {
        if (null == $message) {
            return response()->json(
                $this->generateMessage('Ok', 'Success'),
                $httpCode
            );
        }

        if (is_string($message)) {
            return response()->json(
                $this->generateMessage('Ok', $message),
                $httpCode
            );
        }

        if (is_array($message)) {
            $m = [
                'status' => 'Ok',
                'message' => 'Success',
            ];

            $message = array_merge($m, isset($message['data']) ? $message : ['data' => $message]);

            return response()->json(
                $message,
                $httpCode
            );
        }
    }


    /**
     * Generate response message.
     *
     * @param  string $status
     * @param  mixed $message
     * @return array
     */
    private function generateMessage($status, $message)
    {
        return [
            'status' => $status,
            'message' => $message,
        ];
    }

    /**
     * Get the active driver by bearer token
     *
     * @return \Illuminate\Contracts\Auth\Authenticatable|null|\Illuminate\Database\Eloquent\Model
     */
    protected function getActiveCustomer()
    {
        return auth('api')->user();
    }

    /**
     * Get profile of active driver.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    protected function getActiveCustomerInfo(Request $request)
    {
        $customer = $this->getActiveCustomer();

        return $this->success(
            $customer->toArray()
        );
    }

}