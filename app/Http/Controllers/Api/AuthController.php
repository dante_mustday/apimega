<?php

namespace App\Http\Controllers\Api;

use App\Customer;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class AuthController extends ApiController
{
    /**
     * User (application member) login attempt.
     *
     * @param  Request $request
     * @return \Illuminate\Http\Response
     */
    public function login(Request $request)
    {

        try {
			
            $logins = $request->only(['username', 'password']);
            $validation = Validator::make($request->json()->all(), [
                'username' => 'required',
                'password' => 'required'
            ]);

            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }

            $auth = $this->authenticate($request);

            if (!$auth) {
                $response = array(
					"status"=>"403",
					"message"=>"forbidden",
					"status"=>"ok",
					"error"=>true,
					"message"=>"username / password tidak sesuai"
				);
            }else{
			$auth->username_cust = $auth->first_name_cust!=''?$auth->first_name_cust:$auth->email_cust;
			$response = array(
				"data"=>$auth,
				"status"=>"ok",
				"error"=>false,
				"message"=>"success"
			);
			}
			return $response;

        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
	
	public function password(Request $request)
    {

        try {
			
			$data = $request->json()->all();
			//dd($data);
            $logins = $request->only(['password', 'confirm_password','id']);
            $validation = Validator::make($request->json()->all(), [
				'id'=>'required',
                'password' => 'required',
                'confirm_password' => 'required'
            ]);

            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }

            $auth = $this->changePassword($request);

            if (!$auth) {
                $response = array(
					"data"=>array(),
					"status"=>"403",
					"error"=>true,
					"message"=>"forbidden"
				);
            }else{
			
			$response = array(
				"data"=>array(),
				"status"=>"ok",
				"error"=>false,
				"message"=>"ubah password berhasil"
			);
			}
			return $response;

        } catch (Exception $e) {
            return $this->error($e->getMessage(), $e->getCode());
        }
    }
	
	public function profile(Request $request)
    {
        try {
			
            $logins = $request->only(['id','first', 'last','phone','postal','kota','kecamatan','address','birthday']);
            $validation = Validator::make($request->json()->all(), [
				'id'=>'required',
				'first_name_cust'=>'required',
                'last_name_cust' => 'required',
                'city_cust' => 'required',
				'district_cust' => 'required',
				'postal_cust' => 'required',
				'phone_cust' => 'required',
				'email_cust' => 'required'
            ]);

            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }

            //check email axist
			$data = $request->json()->all();
			if(isset($data['id']) && $data['id']>0){
	            $chk = Customer::where('email_cust',$data['email_cust'])->where("id_cust","!=",$data['id_cust'])->first();
			}else{
	            $chk = Customer::where('email_cust',$data['email_cust'])->first();	
			}
            if(isset($chk->id_cust)){
				$response = array(
					"data"=>array(),
					"status"=>"400",
					"error"=>true,
					"message"=>"email telah digunakan"
				);
				return $response;
            }

            $auth = $this->updateProfile($request);

            if (!isset($auth->id_cust)) {
                $response = array(
					"data"=>array(),
					"status"=>"403",
					"error"=>true,
					"message"=>"forbidden"
				);
            }else{
			
				$response = array(
					"data"=>$auth,
					"status"=>"ok",
				  	"error"=>false,
					"message"=>"profil berhasil di perbaruhi"
				);
			}
			return $response;

        } catch (Exception $e) {
            //return $this->error($e->getMessage(), $e->getCode());

			$response = array(
				"data"=>array(),
				"status"=>"error",
			  	"error"=>true,
				"message"=>$this->error($e->getMessage(), $e->getCode())
			);
			return $response;
        }
    }


    public function reset_password(Request $request)
    {

		$input = $request->json()->all();

		$email = isset($input['username'])?$input['username']:'';
		if($email==''){
			$response = array(
					"status"=>"403",
					"message"=>"forbidden",
					"status"=>"ok",
					"error"=>true,
			);
			return $response;
		}

		$encrypt = sha1("random");
        $user = Customer::whereRaw(" ( email_cust = '".$input['username']."' ) ")->first();
        if(isset($user->id_cust)){
        	//send email
			$response = array(
					"status"=>"403",
					"message"=>"reset password berhasil, cek email",
					"status"=>"ok",
					"error"=>false
			);
			return $response;
        }else{
			$response = array(
					"status"=>"403",
					"message"=>"data tidak ditemukan",
					"status"=>"ok",
					"error"=>true,
			);
			return $response;

        }

    }
	
	private function authenticate(Request $request)
    {
		$data = $request->json()->all();
		$encrypt = sha1($data['password']);
        $user = Customer::where("email_cust",$data['username'])
        ->where("password_cust",$encrypt)
        ->leftJoin('kabupaten_ec','kabupaten_ec.id','customer_ec.city_cust')
        ->leftJoin('ongkir_jne_ec','ongkir_jne_ec.id','customer_ec.district_cust')
        ->select("customer_ec.*",'kabupaten_ec.kabupaten as kota','ongkir_jne_ec.kecamatan')
        ->first();
        return $user;
    }
	
	private function changePassword(Request $request)
    {
		$data = $request->json()->all();
		$id = isset($data['id'])?$data['id']:0;
		$encrypt = sha1($data['confirm_password']);
        //$user = Customer::find($data['id']);

        $user = Customer::where('id_cust',$id)->first();
		if(isset($user->id_cust)){

			$update = array();
			$update['password_cust']= $encrypt;
			DB::table('customer_ec')->where('id_cust',$id)->update($update);
			//$user->password_cust = $encrypt;
			//$user->save();
			return Customer::where('id_cust',$id)->first();
		}else{
			return array();
		}
    
    }
	
	private function updateProfile(Request $request)
    {
		$data = $request->json()->all();
		$id = isset($data['id'])?$data['id']:'';

        //$user = Customer::find($id);
        $user = Customer::where('id_cust',$id)->first();
		if(isset($user->id_cust)){
			$update = array();
			$update['first_name_cust'] 	= isset($data['first_name_cust'])?$data['first_name_cust']:'';
			$update['last_name_cust'] 	= isset($data['last_name_cust'])?$data['last_name_cust']:'';
			$update['phone_cust'] 		= isset($data['phone_cust'])?$data['phone_cust']:'';
			$update['city_cust'] 		= $data['city_cust'];
			$update['district_cust'] 	= $data['district_cust'];
			$update['postal_cust'] 		= $data['postal_cust'];
			$update['address_cust'] 	= $data['address_cust'];
			$update['birthday_cust'] 	= $data['birthday_cust'];
			$update['email_cust'] 	= $data['email_cust'];
			//$user->save();
			DB::table('customer_ec')->where('id_cust',$id)->update($update);
			return Customer::where('id_cust',$id)->first();
		}else{
			return array();
		}
    }

	private function saveProfile(Request $request)
    {
		$data = $request->json()->all();

			if(isset($data['id']) && $data['id']>0){
	            $chk = Customer::where('email_cust',$data['email_cust'])->where("id_cust","!=",$data['id_cust'])->first();
			}else{
	            $chk = Customer::where('email_cust',$data['email_cust'])->first();	
			}
            if(isset($chk->id_cust)){
				$response = array(
					"data"=>array(),
					"status"=>"400",
					"error"=>true,
					"message"=>"email telah digunakan"
				);
				return $response;
            }

        $user = new Customer;
		$update = array();
		$update['username_cust'] 	= isset($data['username_cust'])?$data['username_cust']:'';
		$update['phone_cust'] 	= isset($data['phone_cust'])?$data['phone_cust']:'';
		$update['email_cust'] 	= isset($data['email_cust'])?$data['email_cust']:'';
		$encrypt = sha1($data['password']);
		$update['password_cust'] 	=$encrypt;
		DB::table('customer_ec')->insert($update);

		return array('email_cust'=>$data['email_cust']);

    }

	
	public function register(Request $request)
    {

        try {
        	$response = array();
        	$data = $request->json()->all();

            $logins = $request->only(['phone_cust', 'password','email_cust']);
            $validation = Validator::make($request->json()->all(), [
				'phone_cust'=>'required',
                'password' => 'required',
                'email_cust' => 'required'
            ]);
				
            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }

            //check username password
	        $user = Customer::where('email_cust',$data['email_cust'])->first();
	        
	        if(isset($user->id_cust)){
				$response = array(
					"data"=>array(),
					"status"=>"400",
					"error"=>true,
					"message"=>"email sudah terdaftar"
				);
				return $response;
	        }

            $auth = $this->saveProfile($request);
            if (!isset($auth['email_cust'])) {
                $response = array(
					"data"=>array(),
					"status"=>"403",
					"error"=>true,
					"message"=>"forbidden"
				);
            }else{
				$auth['username_cust'] = $auth['email_cust'];
				$response = array(
					"data"=>$auth,
					"status"=>"ok",
				  	"error"=>false,
					"message"=>"pendaftaran berhasil"
				);
			}
			return $response;

        } catch (Exception $e) {
            //return $this->error($e->getMessage(), $e->getCode());

			$response = array(
				"data"=>array(),
				"status"=>"error",
			  	"error"=>true,
				"message"=>$this->error($e->getMessage(), $e->getCode())
			);
			return $response;
        }
    }

    public function loginGoogle(Request $request)
    {

            $input = $request->json()->all();
            $validation = Validator::make($request->json()->all(), [
                'username' => 'required',
                'email' => 'required',
                'first_name' => 'required'
            ]);
            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }
            $chk = Customer::where("email_cust",$input['email'])->first();
            if(isset($chk->email_cust)){

            }else{
		        $user = new Customer;
				$update = array();
				$update['username_cust'] 	= isset($input['username'])?$input['username']:'';
				$update['email_cust'] 	= isset($input['email'])?$input['email']:'';
				$update['first_name_cust'] 	= isset($input['first_name'])?$input['first_name']:'';
				$update['last_name_cust'] 	= isset($input['last_name'])?$input['last_name']:'';
				DB::table('customer_ec')->insert($update);
            }
            //$auth = DB::table('customer_ec')->where('email_cust',$input['email'])->first();
			$auth = Customer::where("email_cust",$input['email'])
			        ->leftJoin('kabupaten_ec','kabupaten_ec.id','customer_ec.city_cust')
			        ->leftJoin('ongkir_jne_ec','ongkir_jne_ec.id','customer_ec.district_cust')
			        ->select("customer_ec.*",'kabupaten_ec.kabupaten as kota','ongkir_jne_ec.kecamatan')
			        ->first();
			$auth->username_cust = $auth->first_name_cust!=''?$auth->first_name_cust:$auth->email_cust;
			$response = array(
				"data"=>$auth,
				"status"=>"ok",
				"error"=>false,
				"message"=>"success"
			);
			return $response;
    }


}
