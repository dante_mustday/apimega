<?php

namespace App\Http\Controllers\Api;

use App\Banner;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class BannerController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request,$position)
    {
		if($position=='top'){
			$id=1;
		}else if($position=='middle'){
			$id=2;
		}else if($position=='bottom'){
			$id=3;
		}else {
			$id=4;
		}
        $banner = banner::select(DB::raw("CONCAT('banner_ecommerce/',photo_banner) AS image"),'label_banner as label','link_banner as link')
			->where('removed_banner',0)
			->where('position_banner',$id)
			->where('status_banner',1)
			->orderBy('seq_banner','ASC')->get();
		$response = array(
			"data"=>$banner,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }


}
