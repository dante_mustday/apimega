<?php

namespace App\Http\Controllers\Api;

use App\Brand;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class BrandController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request)
    {
    	$input = $request->input();

    	$filter = isset($input['data'])?json_decode($input['data'],true):array();
    	$cat_type = isset($filter['filter']['cat_type'])?$filter['filter']['cat_type']:'';
    	$category_id = isset($filter['filter']['category_id'])?$filter['filter']['category_id']:'';
		
		$where = '';
		if($cat_type=='cat' && $category_id!=''){
			$where .= " AND allcategory_ec.category_id =$category_id ";
		}else if($cat_type=='sub' && $category_id!=''){
			$where .= " AND allcategory_ec.subcategory_id= $category_id ";
		}else if($cat_type=='subsub' && $category_id!=''){
			$where .= " AND allcategory_ec.subsubcategory_id= $category_id ";		
		}else
		if($cat_type=='package'){
			$response = array(
				"data"=>array(),
				"status"=>"ok",
				"message"=>"success"
			);
			return $response;
		}

		if($cat_type=='promo'){
	    	$q = "
	    			SELECT DISTINCT label_brand as name,label_brand as label,link_brand as link 
	    			FROM (`discount_ec`) INNER JOIN `discount_detail_ec` 
					ON `disc_dd`=`id_disc` 
					INNER JOIN `allproduct_ec` ON `var_dd`=`id_var` 
					JOIN brand_ec ON brand_ec.id_brand =  allproduct_ec.brand_inv
					WHERE 
					 discount_ec.end_disc>now() AND `status_disc` = 1 AND `removed_disc` = 0 
					AND `disc_dd` = '$category_id'
					ORDER BY brand_ec.label_brand ASC
	    	";

		}else{
	    	$q = "
	    			select DISTINCT label_brand as name,label_brand as label,link_brand as link
				    FROM variant_ec 
				    JOIN inventory_ec ON id_inv = inv_var
				    JOIN brand_ec ON brand_ec.id_brand =  inventory_ec.brand_inv
				    LEFT JOIN allcategory_ec ON allcategory_ec.id = inventory_ec.category_inv
				    WHERE brand_ec.label_brand IS NOT NULL AND brand_ec.label_brand!=''
				    $where
				    ORDER BY brand_ec.label_brand ASC

	    	";

		}

		$tags = DB::select(DB::raw($q));
		$response = array(
			"data"=>$tags,
			"status"=>"ok",
			"message"=>"success"
		);
		
        return $response;
    }

    public function tags(Request $request)
    {
    	$q = "select DISTINCT inventory_ec.label_inv as name
			    FROM variant_ec 
			    LEFT JOIN inventory_ec ON id_inv = inv_var
			    LEFT JOIN allcategory_ec ON allcategory_ec.id = inventory_ec.category_inv
			    WHERE inventory_ec.label_inv IS NOT NULL AND inventory_ec.label_inv!=''
			    ORDER BY inventory_ec.label_inv ASC
    	";

		$tags = DB::select(DB::raw($q));
		$r=array();
		foreach ($tags as $k => $v) {
			$j = explode(",",$v->name);
			foreach ($j as $key => $value) {
				$d=array();
				$d['name'] = $value;
				$r[] = $d;
			}
		}

		$response = array(
			"data"=>$r,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function loadHome(Request $request)
    {
		$total = $request->input('total');
		$data['brand'] = Brand::select('label_brand as name','img_brand as image','label_brand as label','link_brand as link')
			->where('removed_brand',0)
			->where('status_brand',1)
			->take($total)
			->orderBy('seq_brand','DESC')->get();
			
		return view('load-brand-home', $data);
    }


}
