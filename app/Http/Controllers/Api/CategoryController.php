<?php

namespace App\Http\Controllers\Api;

use App\Category;
use App\Subcategory;
use App\Subsubcategory;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request; 

class CategoryController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
	 
	public function __construct()
    {
		/*header('Access-Control-Allow-Origin: http://megakamera.com');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');*/
	}
	
    public function lists(Request $request)
    {
		///megakamera/backend/uploads/category_ecommerce
        $cat = Category::select('id_cat as id','seq_cat as seq','banner_cat','label_cat as label','link_cat as link',
		DB::raw("CONCAT('backend/uploads/category_ecommerce/',img_cat) as icon"))->where('removed_cat',0)->where('status_cat',1)->orderBy('seq_cat','ASC')->get();
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function categoryDetailWithSub(Request $request,$id)
    {
        $cat = Category::select('id_cat','label_cat as label','link_cat as link')
			->with('subcategory')
			->where('id_cat',$id)
			->first();
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function subcategoryDetailWithSub(Request $request,$id)
    {
        $cat = Subcategory::select('id_scat','label_scat as label','link_scat as link')
			->with('subsubcategory')
			->where('status_scat',1)
			->where('removed_scat',0)
			->where('id_scat',$id)
			->first();
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function subsubcategoryDetail(Request $request,$id)
    {
        $cat = Subsubcategory::select('id_sscat','label_sscat as label','link_sscat as link')
			->where('id_sscat',$id)
			->where('status_sscat',1)
			->where('removed_sscat',0)
			->first();
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function homeload2(Request $request)
    {
		///megakamera/backend/uploads/category_ecommerce
		sleep(1);
		$id = $request->input('lastId');
        $cat = Category::with('relatedinventory.product','subcategory')->where('removed_cat',0)->where('status_cat',1)->where('id_cat','=',$id)->groupBy('seq_cat')->first();
		
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
		
		return $response;
    }
	
	public function homeload(Request $request)
    {
		///megakamera/backend/uploads/category_ecommerce
		sleep(1);
		$id = $request->input('lastId');
        $data['cat'] = Category::with('relatedinventory.product','subcategory')->where('removed_cat',0)->where('status_cat',1)->where('id_cat','=',$id)->groupBy('seq_cat')->first();
		return view('load-home', $data);
		
    }
	
	public function bulk(Request $request)
    {
		$cat = Category::with('subcategory.subsubcategory')->get();
		$response = array(
			"data"=>$cat,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

}
