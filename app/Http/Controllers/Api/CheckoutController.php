<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Discount;
use App\Variant;
use App\Transaction;
use App\Helpers\ParseCI;
use App\Customer;
use App\Transactiondetail;
use Mail;
use DB;
use Exception;
use Illuminate\Http\Request;
use parse_str;
use Illuminate\Support\Facades\Schema;

class CheckoutController extends ApiController
{

    public  $l_delim = '{';
    public  $r_delim = '}';

	public function submit(Request $request)
    {
    	$input = $request->json()->all();
    	$transaction = new Transaction;

        if(!isset($input['customer_data'])){
            $result['error'] = true;
            $result['msg'] = 'check koneksi kamu';
            $result['id'] = 0;
            return $result;
            die;
        }

    	$cart = array();
    	$id_cust = '';
        $sell_point = 0;
    	foreach($input as $key=>$value){
    		if($key=='cart'){
    			foreach ($value as $k => $c) {
                    
    				$d = new Transactiondetail;
    				foreach ($c as $ck => $cv) {
                        // use poin
                        if($ck=='sell_point'){
                            $sell_point += $c['sell_point'];
                        }
    					if($ck=='cust_dt'){
    						$id_cust = $cv;
    					}
			            if(Schema::hasColumn($d->getTable(), $ck)){ 
			                $d->$ck = $cv;
			            }
    				}
    				$cart[] = $d; 
    			}
    			continue;
    		}
            if(Schema::hasColumn($transaction->getTable(), $key)){ 
                $transaction->$key = $value;
            }
        }
        $customer = Customer::find($id_cust);
        foreach($input['customer_data'] as $key=>$value){
            if($key=='created_time_cust' || $key=='updated_time_cust' || $key=='password_cust'){
                continue;
            }
            if(Schema::hasColumn($customer->getTable(), $key)){ 
                $customer->$key = $value;
            }
        }


        $bank = DB::table('bank_ec')->where("status_bank",1)->get();
        DB::beginTransaction();
        try{
        	$transaction->invoice_number = "INV-".date("ymdhis");
        	$transaction->created_time_trans = date('Y-m-d H:i:s');
        	//$transaction->save();
        	foreach ($cart as $key => $value){
        		$value->trans_dt = $transaction->id_trans;
        		//$value->save();
        	}
            $customer->save();
            DB::commit();
            $result['data']=array('invoice_number'=>$transaction->invoice_number,'bank'=>$bank);
            $result['msg'] = 'berhasil';
            $result['profile'] = $customer;
            $kota= DB::table('kabupaten_ec')->where('id',$customer->city_cust)->first();
            $result['profile']->kota = isset($kota->id)?$kota->kabupaten:'';
            $result['status'] = 'ok';
            //$this->sendeMail($transaction->invoice_number);
        }catch(Exception $e){
            $result['error'] = true;
            $result['msg'] = $e;
            $result['id'] = 0;
            DB::rollback();   
        };
        return $result;


    }
	
	private function updatestock(){
		return false;
	}

	public function getVoucher($code)
	{
		$data = DB::table("voucher_ec")
		->where('code_voc',$code)
		->where('status_voc',1)
		->where('removed_voc',0)
		->first();

		if(isset($data->code_voc)){
			$data->min_voc = number_format($data->min_voc);
			$response = array(
						"data"=>$data,
						"status"=>"ok",
						"error"=>true,
						"message"=>"success"
			);
		}else{
			$response = array(
						"data"=>array(),
						"status"=>"error",
						"error"=>true,
						"message"=>"masukan kode voucher yang benar"
			);

		}
		return $response;

	}


    public function sendeMail($invoice_number)
    {
        $transaction = DB::table('transaction_ec')
                ->where('invoice_number',$invoice_number)
                ->first();
        $transaction =  json_decode(json_encode($transaction), true);

        $header = DB::table('email_ec')->where('id_email',1)->first(); //header
        $footer = DB::table('email_ec')->where('id_email',2)->first(); //footer
        $content = DB::table('email_ec')->where('id_email',4)->first(); //footer
        $thank = DB::table('email_ec')->where('id_email',3)->first(); //thank you
        $trans_detail = DB::table("transaction_detail_ec")
                    ->join("allproduct_ec","allproduct_ec.id_var","transaction_detail_ec.var_dt")
                    ->select('transaction_detail_ec.sku_dt as sku', 
                      'transaction_detail_ec.name_dt as name', 
                      'transaction_detail_ec.variant_dt as variant', 
                      'transaction_detail_ec.price_dt as price', 
                      'transaction_detail_ec.qty_dt as qty', 
                      'transaction_detail_ec.subtotal_dt as subtotal',
                      'allproduct_ec.permalink_inv as permalink',
                      'allproduct_ec.id_var')
                    ->where("trans_dt",$transaction['id_trans'])->get();

        $trans_details= json_decode(json_encode($trans_detail), true);

        $the_template = $content->content_email;

        $invoice_number=$transaction['invoice_number'];

        $r=array();
        $r['customer_first_name'] = $transaction['first_name_trans'];
        $r['customer_last_name'] = $transaction['last_name_trans'];
        $r['customer_name'] = $transaction['first_name_trans']." ".$transaction['last_name_trans'];
        $r['customer_address'] = $transaction['address_trans'];
        $r['customer_phone'] = $transaction['phone_trans'];
        $r['customer_email'] = $transaction['email_trans'];
        $r['customer_city'] = $transaction['city_trans'];
        $r['customer_postal'] = $transaction['postal_trans'];
        $r['transaction_shipping'] = number_format($transaction['shipping_trans']);
        $r['transaction_tax'] = $transaction['tax_trans'];
        $r['transaction_total'] = number_format($transaction['total_trans']);
        $r['transaction_total_tax'] = number_format($transaction['total_tax_trans']-$transaction['total_trans']);
        $r['datetime'] = date("d/m/Y H:i:s");
        $discount = $transaction['amount_discount_trans'];
        if(!empty($discount)){
            $r['transaction_with_tax'] = number_format($transaction['total_tax_trans']-$discount)." ( Discount ".number_format($discount)." )";
           
        } else {
            $r['transaction_with_tax'] = number_format($transaction['total_tax_trans']);
            
        }
        $r['transaction_insurance']=number_format($transaction['insurance_trans']);
                $r['transaction_amount']=number_format($transaction['grand_total_trans']);
                $r['transaction_method'] = $transaction['method_trans'];
                $r['transaction_billing'] = number_format($transaction['grand_total_trans']);
                $r['transaction_info_shipping'] = $transaction['info_shipping_trans'];
                $r['invoice_number'] = $transaction['invoice_number'];
                $r['transaction_id']     = $transaction['id_trans'];
        $details=array();
        foreach ($trans_details as $key => $value) {
            $details[$key]['name'] = "<a href='".'http://megakamera.com/index.php/product/'.$trans_details[$key]['permalink'].'/'.$trans_details[$key]['id_var']."' target='_blank'>".$trans_details[$key]['name']."</a>";
            if( isset($trans_details[$key]['discount']) && $trans_details[$key]['discount']!=0){
                $details[$key]['price'] = number_format($trans_details[$key]['price']-$trans_details[$key]['discount']);
            }else{
                $details[$key]['price'] = number_format($trans_details[$key]['price']);
            }
            $details[$key]['sku'] = $trans_details[$key]['sku'];
            $details[$key]['subtotal'] = number_format($trans_details[$key]['subtotal']);
            $details[$key]['qty'] = number_format($trans_details[$key]['qty']);
            
        }
        $data = DB::table("global_param_ec")->get();
        $company = array();
        foreach($data as $row){
            if($row->type_gp != "image"){
            $company[$row->name_gp] = $row->value_gp;
            }else{
                $company[$row->name_gp] = "backend/uploads/".$row->value_gp;
            }
        }

        $bank_detail = DB::table('bank_ec')
        ->select('name_bank as name','branch_bank as branch','user_bank as user','no_bank as bank')
        ->where('status_bank',1)->get();
        $bank_detail =  json_decode(json_encode($bank_detail), true); // bank

        $r['transaction_detail'] = $details;
        $r['bank_detail'] = $bank_detail;
        $r['status'] = $transaction['status_trans'];
        $r['invoice_number'] = $transaction['invoice_number'];

        $r['site_title'] ="megakamera.com";
        $r['logo_image'] = "http://megakamera.com/".$company['site_logo'];
        $r['datetime'] = date("d M Y - H:i");


        //echo ParseCI::get();
        //echo "<br>";
        $template = $this->_parse($the_template,$r,true);
        //echo $template;
        $this->sendGmail($r['customer_email'],$template,$company,$r);
    }

    public function sendGmail($to,$template,$company,$data)
    {

        $from = "do-not-reply@megakamera.co.id";//"iagus048@gmail.com";
        //$to = "mft.aang@gmail.com";
        
        $title = $company['site_title'];
        $admin_email = $company['admin_email'];
        $subject = $company['site_title']." - your transaction status #".$data['invoice_number']." : ".$data['status'];
        $user = array('to'=>$to,"subject"=>$subject,"template"=>$template,
            "from" =>$from,
            "title"=>$title,
            "admin_email"=>$admin_email
        );

        Mail::send([],[], function ($message) use ($user) {
            $message->from($user['from'], $user['title']);
            $message->to($user['to'])->subject($user['subject']);
            $message->bcc("info@megakamera.com")->bcc($user['admin_email']);
            $message->setBody($user['template'], 'text/html');
        });
    }
    //parse proses
    public function _parse($template, $data, $return = FALSE)
    {
        if ($template === '')
        {
            return FALSE;
        }

        $replace = array();
        foreach ($data as $key => $val)
        {
            $replace = array_merge(
                $replace,
                is_array($val)
                    ? $this->_parse_pair($key, $val, $template)
                    : $this->_parse_single($key, (string) $val, $template)
            );
        }

        unset($data);
        $template = strtr($template, $replace);

        if ($return === FALSE)
        {
            $template .= $template;
            //$this->CI->output->append_output($template);
        }

        return $template;
    }
    public function _parse_single($key, $val, $string)
    {
        return array($this->l_delim.$key.$this->r_delim => (string) $val);
    }
    public function _parse_pair($variable, $data, $string)
    {
        $replace = array();
        preg_match_all(
            '#'.preg_quote($this->l_delim.$variable.$this->r_delim).'(.+?)'.preg_quote($this->l_delim.'/'.$variable.$this->r_delim).'#s',
            $string,
            $matches,
            PREG_SET_ORDER
        );

        foreach ($matches as $match)
        {
            $str = '';
            foreach ($data as $row)
            {
                $temp = array();
                foreach ($row as $key => $val)
                {
                    if (is_array($val))
                    {
                        $pair = $this->_parse_pair($key, $val, $match[1]);
                        if ( ! empty($pair))
                        {
                            $temp = array_merge($temp, $pair);
                        }

                        continue;
                    }

                    $temp[$this->l_delim.$key.$this->r_delim] = $val;
                }

                $str .= strtr($match[1], $temp);
            }

            $replace[$match[0]] = $str;
        }

        return $replace;
    }

    //end parse proses

}