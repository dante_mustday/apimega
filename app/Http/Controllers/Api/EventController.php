<?php

namespace App\Http\Controllers\Api;

use App\Banner;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class EventController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request)
    {
        $data = DB::table("event_ec")
			->where('removed',0)
			->where('status',1)
			//->whereDate('date','>=',date('Y-m-d'))
			->orderBy('id','DESC')->limit(10)->get();

		$r=array();
		foreach ($data as $k => $v) {
			$desc = '';
			if($v->type==1){
				$desc = date('d/m/Y', strtotime($v->date))." | Pukul : ".$v->time." WIB";
				$desc .="<br>".date('d/m/Y',strtotime($v->start_date))." S/d ".date('d/m/Y',strtotime($v->end_date));
			}else{
				$desc = date('d/m/Y', strtotime($v->date))." | Pukul : ".$v->time." WIB";
			}

			$v->desc = $desc;
			$v->src = "event_ecommerce/".$v->thumbnail;
			$r[]= $v;
		}
		$response = array(
			"data"=>$r,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

    public function detail($id,Request $request)
    {
        $v = DB::table("event_ec")
			->where('id',$id)->first();


		if($v->type==1){
				$date = "Pukul : ".$v->time." WIB";
				$date .="<br>".date('d/m/Y',strtotime($v->start_date))." S/d ".date('d/m/Y',strtotime($v->end_date));
		}else{
				$date = date('d/m/Y', strtotime($v->date))." | Pukul : ".$v->time." WIB";
		}
		$desc = "
			<table width='100%'>
				<tr>
					<td align='center'>Tempat:</td>
				</tr>
				<tr>
					<td  align='center'><b>".$v->place."</b></td>
				</tr>
				<tr>
					<td  align='center'>&nbsp;</td>
				</tr>
				<tr>
					<td  align='center'>Tanggal:</td>
				</tr>
				<tr  align='center'>
					<td><b>".$date."</b></td>
				</tr>
			</table>
			<br>
			<div>
				$v->text
			</div>
		";

		$v->desc = $desc;
		$v->src = "event_ecommerce/".$v->thumbnail;
		$response = array(
			"data"=>$v,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }


	public function requestItem(Request $request)
    {

        try {
        	$response = array();
        	$data = $request->json()->all();

            $validation = Validator::make($request->json()->all(), [
				'fullname'=>'required',
                'email' => 'required',
                'telephone' => 'required',
                'barang' => 'required',
            ]);
				
            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }


            $auth = DB::table('request_item_ec')->insert($request->json()->all());
            // send email
			
			$response = array(
					"data"=>$auth,
					"status"=>"ok",
				  	"error"=>false,
					"message"=>"request item berhasil terkirim"
			);
			
			return $response;

        } catch (Exception $e) {
            //return $this->error($e->getMessage(), $e->getCode());

			$response = array(
				"data"=>array(),
				"status"=>"error",
			  	"error"=>true,
				"message"=>$this->error($e->getMessage(), $e->getCode())
			);
			return $response;
        }
    }

    public function about()
    {
		$page = DB::table('page_ec')->where("permalink_page",'tentang-megakamera')->where('status_page',1)->get();
		return array('data'=>isset($page[0])?$page[0]:array());
    }


}
