<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\DB;

use App\User;
use Image;
use Storage;

class ExampleController extends ApiController
{
    private $entity;
    public function __construct(){
        $this->entity = new User;
    }

    public function blog_category(Request $request)
    {
        $input = $request->input();

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < 5; $i++) { 
            $data[]=array(
                "id"=> $i."--",
                "count"=> $i*2,
                "name"=> "Kamera",
                "slug"=> "foto-fsd",
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    public function blog(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'publish_date';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) { 
            $data[]=array(
                "id"=> $i."--".$page,
                "title"=> "Every need notepad",
                "writer"=> "Agung",
                'time'=>"10 Maret 2018",
                "category"=> "MOTIVASION, FOOD",
                "content_prev"=> "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica", //prev remove html tag
                "slug"=> "blog-link-hrfe",
                "img"=>"https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    
    public function blogDetail(Request $request,$id)
    {
        $return = array();
        $data=array(
                "id"=> 'dbplus_add(relation, tuple)',
                "title"=> "Every need notepad",
                "writer"=> "Agung",
                'time'=>"10 Maret 2018",
                "category"=> "MOTIVASION, FOOD",
                "content"=> '
                    <p><a href="https://www.bola.com" target="_blank"><i class="i-location-bola"></i></a> Jakarta - Perenang Indonesia, Jendi Pangabean, meraih medali perak pada nomor 100 meter putra gaya kupu-kupu S9, Jumat (12/10/2018). Medali tersebut menjadi yang keempat diraih Jendi di <strong><a href="https://www.bola.com/asian-para-games">Asian Para Games 2018</a></strong>.</p>
                    <p>Pada perlombaan tersebut, Jendi finis dengan catatan waktu 1 menit 6,80 detik. Perenang 27 tahun itu hanya kalah 1,40 detik dari wakil Jepang, Kubo Daiki.</p>
                    <p>"Senang dapat medali keempat untuk Indonesia. Bahagia bisa dapat perak. Memang ini bukan yang ditargetkan, akan tetapi saya bersyukur bisa meraih perak," kata Jendi kepada wartawan seusai pertandingan.</p>
                    <p>Sebelumnya, Jendi sudah meraih tiga medali lain di Asian Para Games 2018. Jendi meraih perunggu di nomor 100 meter gaya bebas S9, emas di nomor 100 meter gaya punggung S9, dan perunggu di nomor 4x100 meter gaya ganti putra.</p>
                    <p>"Puas sekali dengan pencapaian ini karena menjadi yang tertinggi selama kariernya saya di Asian Para Games. Untuk pentas Asia pun ini pencapaian yang luar biasa," ujar Jendi.</p>
                ',
                "slug"=> "blog-link-hrfe",
                "img"=>"https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
            );

        $return['message'] = "success";
        $return['status'] = "ok";
        $return['data']=$data;
        return $return;
    }


    public function ambasador(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'publish_date';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) { 
            $data[]=array(
                "id"=> $i."--".$page,
                'slug'=>"amsda-sdsa",
                "profilePhoto"=> "https://www.marketaccesstransformation.com/wp-content/uploads/2018/01/profile-clipart-default-user-9.jpg",
                "name"=> "Agung",
                'about'=>"Very Hard Glass With Germany Special Coating and Strach-Resisten", // as prev remove html tag
                "imgs"=> array(  // create 2 type original and thumnail
                    array(
                        'original' =>'https://cdn.thewirecutter.com/wp-content/uploads/2016/05/micro43lenses-top-2x1-fullres-3656-1024x512.jpg',
                        'thumbnail' =>'https://cdn.thewirecutter.com/wp-content/uploads/2016/05/micro43lenses-top-2x1-fullres-3656-1024x512.jpg',
                    ),
                    array(
                        'original' =>'http://react-responsive-carousel.js.org/assets/1.jpeg',
                        'thumbnail' =>'http://react-responsive-carousel.js.org/assets/1.jpeg',
                    )
                )
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    
    public function ambasadorDetail(Request $request,$id)
    {
        $return = array();
        $data=array(
                "id"=> "asds--",
                'header_img'=>'http://localhost:3000/static/media/b.e56076ff.jpg',
                'slug'=>"amsda-sdsa",
                'title'=>"KASE IRLAND BRAND AMMBASADOR",
                'fb'=>'https://en.wikipedia.org/wiki/Camera_operator',
                'ig'=>'https://en.wikipedia.org/wiki/Camera_operator',
                "profilePhoto"=> "https://www.marketaccesstransformation.com/wp-content/uploads/2018/01/profile-clipart-default-user-9.jpg",
                "name"=> "Agung",
                'main_gear'=>array(
                    'NIKKON D850 | K 170 SERIES',
                    'BLACK ROLL'
                ),
                'gear'=>array(
                    array(
                        'K170 SERIES FILTER BAG','K170 FILTER HOLDER','ND 100','NG 64'
                    ),
                    array(
                        'NIKKON d805','NIKKON D3','ND 100','NG 64'
                    ),
                    array(
                        'BENDO 2 TRIPOD'
                    ),
                ),
                'about'=>'
                    <p><a href="https://www.bola.com" target="_blank"><i class="i-location-bola"></i></a> Jakarta - Perenang Indonesia, Jendi Pangabean, meraih medali perak pada nomor 100 meter putra gaya kupu-kupu S9, Jumat (12/10/2018). Medali tersebut menjadi yang keempat diraih Jendi di <strong><a href="https://www.bola.com/asian-para-games">Asian Para Games 2018</a></strong>.</p>
                    <p>Pada perlombaan tersebut, Jendi finis dengan catatan waktu 1 menit 6,80 detik. Perenang 27 tahun itu hanya kalah 1,40 detik dari wakil Jepang, Kubo Daiki.</p>
                    <p>"Senang dapat medali keempat untuk Indonesia. Bahagia bisa dapat perak. Memang ini bukan yang ditargetkan, akan tetapi saya bersyukur bisa meraih perak," kata Jendi kepada wartawan seusai pertandingan.</p>
                    <p>Sebelumnya, Jendi sudah meraih tiga medali lain di Asian Para Games 2018. Jendi meraih perunggu di nomor 100 meter gaya bebas S9, emas di nomor 100 meter gaya punggung S9, dan perunggu di nomor 4x100 meter gaya ganti putra.</p>', // as 
                "imgs"=> array(
                    array(
                        'src' =>'https://cdn.thewirecutter.com/wp-content/uploads/2016/05/micro43lenses-top-2x1-fullres-3656-1024x512.jpg',
                        'width'=>3,'height'=>2 // for list imge
                    ),
                    array(
                        'src' =>'http://react-responsive-carousel.js.org/assets/1.jpeg',
                        'width'=>3,'height'=>2
                    ),
                )
            );

        $return['message'] = "success";
        $return['status'] = "ok";
        $return['data']=$data;
        return $return;
    }

    public function dealer(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'id';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) { 
            $data[]=array(
                "id"=> $i."--".$page,
                'nama'=>"BENGKEL LAS DAN BUBUT MANDIRI",
                "alamat"=> "Jl. Adi Sucipto 33",
                "provinsi"=> "JAWA TENGAH",
                'kota'=>"KLATEN",
                'telpon'=>'0271-727005',
                'jenis_layanan'=>'Bengkel'
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    public function video(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'id';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) { 
            if($i==0){
                $title = 'asdsad';
            }else{
                $title='FULL VIDEO..! Sang penguasa sombong yang menjadi pecundang substitle indonesia Cerita Nurmagomedov Sukses Membantai Sang Penista Agama Islam di Ring';
            }
            $data[]=array(
                "id"=> $i."--".$page,
                'title'=>$title,
                "category"=> "UCF",
                "src"=> 'https://www.youtube.com/embed/r07Suoqcs08',
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }


    public function promo(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'id';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) {
            $data[]=array(
                "id"=> $i."--".$page,
                'name'=>'promo name',
                "slug"=> "promo-slug",
                "period"=> "10 Sep 2018 S/d 31 Oct 2018",
                "img"=> 'http://megakamera.com/backend/uploads/discount_ecommerce/XH11.jpg',
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    public function promoDetail(Request $request,$id)
    {
        $return = array();
        $data=array(
                "id"=> 'sdasdsa',
                'name'=>'promo name',
                "slug"=> "promo-slug",
                "period"=> "10 Sep 2018 S/d 31 Oct 2018",
                "img"=> 'http://megakamera.com/backend/uploads/discount_ecommerce/XH11.jpg',
            );

        $return['message'] = "success";
        $return['status'] = "ok";
        $return['data']=$data;
        return $return;
    }


    public function register_app_id(Request $request)
    {
        $app_id = 'kasecom';
        $signature = $request->header('Signature');  // for profile
        $input = $request->input(); // cardsArrya,

        //encode appid 
        $status='error';
        $msg = 'tidak valid';
        $data = array();
        $check_data = base64_decode($signature);
        if (strpos($check_data, $app_id) !== false) {
            $status='ok';
            $msg = 'valid';
            //if not error get data
            // get setting dta to save local store
            $data = array(
                            'menu'=>$this->getMenu(),
                            'logo'=>'/logo.png',
                            'about-page'=>$this->getPage('about-page'),
                            'contact-page'=>$this->getPage('contact-page'),
                            'fb'=>'https://www.facebook.com/Juventusfcid/?brand_redir=171522852874952',
                            'twirter'=>'https://www.facebook.com/Juventusfcid/?brand_redir=171522852874952',
                            'ig'=>'https://www.facebook.com/Juventusfcid/?brand_redir=171522852874952',
                    ); 

        }
        //$return['c']=$check_data;
        //$return['s'] = strpos($app_id, $check_data);
        $return['status']=$status;
        $return['msg']=$msg;
        $return['data']=$data;
        
        //$return['status']='not';
        //$return['message']='email dan pwd tidak cocok';
        //$return['input'] = $input;
        return $return;
    }

    function getPage($type){
        // type page contact, about etc 
        $d = '<div class="shopper-info">
                    
                    <!--<h1>Tentang Megakamera</h1>
                    <br/>-->
                    <p>Kami merupakan Store photo dan videografi yang sudah beroperasi sejak 1994. Mengikuti Perkembangan jaman kamipun masuk ke dunia online dengan Brand Megakamera.com yang dimulai sejak Tahun 2013. Megakamera.com senantiasa mencoba memuaskan pelanggan dengan moto ACT (Aman Cepat Tepat). Meggakamera.com bekerjasama dengan beberapa Marketplace yang mempermudah dalam melakukan transaksi Online.&nbsp;</p>
                <p><br>Mengapa berbelanja melalui Megakamera.com</p>
                <p><br>Cicilan 0% Semua Produk<br>Berbelanja lebih nyaman dengan fasilitas cicilan 0%. info lengkap.</p>
                <p>Pengiriman Produk di hari yang sama.<br> Semua Orderan yang masuk ke Megakamera.com sebelum jam 17.00 akan kami proses di hari yang sama.</p>
                <p><br>Gratis Pengiriman Seluruh Indonesia<br>Lokasi Anda di Luar Jakarta tapi ingin bebas ongkos kirim. Megakamera.com memberikan layanan Gratis Pengiriman ke seluruh Indonesia. info lengkap.<br><br>Customer Care&nbsp;021-62301600<br>Ingin Bertanya seputar produk, cara penggunaan, ketersediaan barang, transaksi, claim garansi, dan sebagainya, silakan hubungi Megakamera.com di 021-62301600 atau Whatsapp 089-888-20055. info lengkap.<br><br>Jaminan Pengembalian Produk<br>Untuk kepuasan dalam berbelanja, Apabila barang yang konsumen terima dalam keadaan Rusak, tidak lengkap, dan atau tidak sesuai dengan Pesanan, jangan khawatir kami akan menggantikan sesuai dengan yang dipesan. info lengkap.<br><br>Sistem pembayaran yang mudah, aman dan terpercaya<br>Kami senantiasa mengembangkan pilihan metode pembayaran yang bervariasi dan aman.<br><br>Promo<br>Nikmati Promo-promo menarik di Megakamera.com setiap bulannya. berlangganan newsletter melalui Megakamera.com atau terkoneksi ke social media seperti Instagram dan Facebook Megakamera, membantu Konsumen mendapatkan informasi terkini. info&nbsp; lengkap.<br><br><br>FAQ<br>Q: Apakah Megakamera.com memiliki toko offline?<br>A: Ya kita memiliki Toko offline. Pembelian bisa melakukan transaksi melalui website Megakamera.com atau berbelanja secara langsung di Store Megakamera.com<br><br>Q: Dimana alamat store Megakamera.com?<br>A: Megakamera.com berlamat di Jl. Gunung Sahari Raya No.26 Jakarta Pusat 10720. <a title="Alamat Megakamera.com" href="http://103.232.33.59/index.php/pages/hubungi-kami" target="_blank">klik disini</a><br><br>Q: Apakah alamat Megakamera.com terdaftar di Google Maps?<br>A: Ya, Alamat Megakamera.com terdaftar di google Maps. Tinggal masukkan pencarian Megakamera, alamat akan muncul.<br><br>Q: Apakah setiap pembelian barang melalui Megakamera.com mendapatkan pelatihan?<br>A: Salah satu bentuk layanan terbaik&nbsp;kami adalah layanan pelatihan.&nbsp;Setiap belanja di Megakamera.com kami memberikan pelatihan penggunaan Kamera dan accessories, setiap harinya di jam operational.&nbsp;<br><br>Q: Apakah pelatihan ini berbayar?<br>A: Pelatihan sifatnya Free, kecuali workshop dan training yang sebelumnya kita informasikan berbayar.</p>
                </div>';
        return $d;

    }
    function getMenu(){
        $d = array(
            array('name'=>'Home','link'=>'/home'),
            array('name'=>'Product','link'=>'/product'),
            array('name'=>'Promo','link'=>'/promo'),
            array('name'=>'Video','link'=>'/video'),
            array('name'=>'Ambassador','link'=>'/ambassador'),
            array('name'=>'Blog','link'=>'blog'),
            array('name'=>'Dealer','link'=>'dealer'),
            array('name'=>'Contact','link'=>'/pages/contact'),
            array('name'=>'About','link'=>'/pages/about'),
        );
        return $d;
    }



    public function productCategory(Request $request)
    {
        $input = $request->input();

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < 5; $i++) { 
            $data[]=array(
                "id"=> $i."--",
                "count"=> $i*2,
                "name"=> "Kamera",
                "slug"=> "foto-fsd",
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    public function product(Request $request)
    {
        $input = $request->input();
        $page = isset($input['page'])?$input['page']:1;
        $limit = isset($input['limit'])?$input['limit']:6;
        $orderBy = isset($input['orderBy'])?$input['orderBy']:'publish_date';
        $sortBy = isset($input['sortBy'])?$input['sortBy']:'desc';
        $category_slug = isset($input['cat'])?$input['cat']:'';
        $q = isset($input['q']) && $input['q']!=''?$input['q']:'';

        //example 
        $data = array();
        $totalData = 18;
        for ($i=0; $i < $limit; $i++) { 
            $data[]=array(
                "id"=> $i."--".$page,
                "title"=> "Every need notepad",
                "writer"=> "Agung",
                'time'=>"10 Maret 2018",
                "category"=> "MOTIVASION, FOOD",
                "content_prev"=> "Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging across all continents except Antarctica", //prev remove html tag
                "slug"=> "blog-link-hrfe",
                "img"=>"https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
            );
        }
        $return['message'] = "success";
        $return['status'] = "ok";
        $return['total'] = $totalData;
        $return['data'] = $data;
        return $return;
    }

    
    public function productDetail(Request $request,$id)
    {
        $return = array();
        $data=array(
                "id"=> 'dbplus_add(relation, tuple)',
                "title"=> "Every need notepad",
                "writer"=> "Agung",
                'time'=>"10 Maret 2018",
                "category"=> "MOTIVASION, FOOD",
                "content"=> '
                    <p><a href="https://www.bola.com" target="_blank"><i class="i-location-bola"></i></a> Jakarta - Perenang Indonesia, Jendi Pangabean, meraih medali perak pada nomor 100 meter putra gaya kupu-kupu S9, Jumat (12/10/2018). Medali tersebut menjadi yang keempat diraih Jendi di <strong><a href="https://www.bola.com/asian-para-games">Asian Para Games 2018</a></strong>.</p>
                    <p>Pada perlombaan tersebut, Jendi finis dengan catatan waktu 1 menit 6,80 detik. Perenang 27 tahun itu hanya kalah 1,40 detik dari wakil Jepang, Kubo Daiki.</p>
                    <p>"Senang dapat medali keempat untuk Indonesia. Bahagia bisa dapat perak. Memang ini bukan yang ditargetkan, akan tetapi saya bersyukur bisa meraih perak," kata Jendi kepada wartawan seusai pertandingan.</p>
                    <p>Sebelumnya, Jendi sudah meraih tiga medali lain di Asian Para Games 2018. Jendi meraih perunggu di nomor 100 meter gaya bebas S9, emas di nomor 100 meter gaya punggung S9, dan perunggu di nomor 4x100 meter gaya ganti putra.</p>
                    <p>"Puas sekali dengan pencapaian ini karena menjadi yang tertinggi selama kariernya saya di Asian Para Games. Untuk pentas Asia pun ini pencapaian yang luar biasa," ujar Jendi.</p>
                ',
                "slug"=> "blog-link-hrfe",
                "img"=>"https://material-ui.com/static/images/cards/contemplative-reptile.jpg"
            );

        $return['message'] = "success";
        $return['status'] = "ok";
        $return['data']=$data;
        return $return;
    }


}
