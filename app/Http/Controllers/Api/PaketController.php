<?php

namespace App\Http\Controllers\Api;

use App\Paket;
use App\Variants;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class PaketController extends ApiController
{
	public function __construct()
	{
		parent::__construct();
	}
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
    {
		$result = paket::with('details')
			->with('details.variant')
			->where('status_pkt',1)
			->where('removed_pkt',0)
			->take(5)->get();
		$return = array(
			"data"=>$result,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
	}
	public function category()
	{
		$result = DB::table('packet_category_ec')
			->where('status',1)
			->where('removed',0)->get();
		$r = array();

		foreach ($result as $k => $v) {
			$v->src = "packet_category_ecommerce/".$v->img;

			$r[] = $v;
		}
		$return = array(
			"data"=>$r,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
	}
	 
    public function lists(Request $request)
    {
		$total = 0;	
    	$input = $request->input();
    	$tags = isset($input['tags']) && $input['tags']!=''?explode(",",$input['tags']):'';
    	$brand = isset($input['brand']) && $input['brand']!=''?explode(",",$input['brand']):'';
    	$sortBy = isset($input['sortBy']) && $input['sortBy']!=''?$input['sortBy']:'name_inv';
    	$order = isset($input['order']) && $input['order']!=''?$input['order']:'asc';
    	$category_filter = isset($input['category_filter']) && $input['category_filter']!=''?$input['category_filter']:'category_filter';
    	$q = isset($input['q']) && $input['q']!=''?$input['q']:'';

    	$where ='';
    	if($q!=''){

    	}

    	if($category_filter!=''){
    		$where .=" and p.category_id = $category_filter ";
    	}

    	$query = "
    		select p.*,
		(select stock_var from all_package where id_pkt=p.id_pkt) stock,
		(select src_img from paket_image_ec where id_paket=p.id_pkt and cover='1' limit 1) images 
		from packet_ec p
		where p.status_pkt=1
		and p.removed_pkt=0
		$where
    	";


    	switch ($sortBy) {
    		case 'Nama':
    			$sortBy ='label_pkt';
    			break;
    		case 'Produk Terpopuler':
    			$sortBy ='variant_ec.seq_var';
    			break;
    		case 'Produk terbaru':
    			$sortBy ='p.created_time_pkt';
    			break;
    		case 'Stok':
    			$sortBy ='stock';
    			break;
    		case 'Harga Tertinggi':
    			$sortBy ='price_pkt';
    			break;
    		case 'Harga Terendah':
    			$sortBy ='price_pkt';
    			break;
    		default:
    			$sortBy = 'label_pkt';
    			break;
    	}
		
		$data = DB::select(DB::raw($query." ORDER BY $sortBy $order LIMIT $this->start, $this->limit "));
		$total = COUNT(DB::select(DB::raw($query."  ")));

		$r = array();
		foreach ($data as $k => $v) {
			$v->name = $v->label_pkt;
			$v->sell_price = $v->price_pkt;
			$v->price = $v->normal_price_pkt;
			$v->is_package = 1;
			$v->variant = 'package';
			$v->inventory = $v->id_pkt;
			$v->image = "packet_ecommerce/".$v->images;
			$v->label = '';
			$r[] = $v;
		}
		$return = array(
			"data"=>$r,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function detail(Request $request,$id_var)
    {

		$value = DB::table('packet_ec')
		->where("packet_ec.status_pkt",1)
		->where('packet_ec.id_pkt',$id_var) 
		->first();
		$result = array();


		$fimages = DB::table('paket_image_ec')
					->where('id_paket',$id_var)
					->where("cover","1")
					->first();
		if(isset($fimages->id_img)){
			$images = DB::table('paket_image_ec')
					->where('id_paket',$id_var)
					->where('id_img','!=',$fimages->id_img)
					->orderBy("id_img","desc")
					->get();
		}else{
			$images = DB::table('paket_image_ec')
					->where('id_paket',$id_var)
					->orderBy("id_img","desc")
					->get();
		}
		$collection = array();
		foreach($images as $image){
				$collection[]=array(
					"label"=> '',
					"image"=>'packet_ecommerce/'.$image->src_img
				);
		} 
		$sell_price = $value->price_pkt;
		$price = $value->normal_price_pkt;
		$is_package = 1;
		$variant_option = array();


		$inventory = DB::table('packet_ec')
        		->join("packet_detail_ec","pkt_pktd","id_pkt")
         		->join("allproduct_ec","var_pktd","allproduct_ec.id_var")
         		->join("variant_ec","variant_ec.id_var","allproduct_ec.id_var")
         		->select("allproduct_ec.id_inv","allproduct_ec.id_var","id_pkt","variant_ec.unit_shipping_var as weight")
         		->where('id_pkt',$id_var)
         		->where('status_pkt',1)->get();


        $stock = DB::select(DB::raw("select stock_var from all_package where id_pkt= ".$id_var));
        $stock = isset($stock->stock_var)?$stock->stock_var:0;
		$order_status = '';
		$order_status_style = '';
			if($stock==0){
				$order_status = 'Habis';
				$order_status_style ='red';
			}else{ 
				$order_status = 'In Stok';
				$order_status_style ='green';
			} 
			$result = array(
				"name"			=> @$value->label_pkt,
				"realname"			=> @$value->label_pkt,
				"deskripsi"		=> $value->desc_pkt,
				"headline_inv" =>$value->highlight_pkt,
				"box"			=> $value->witb_pkt,
				"inventory_data" =>$inventory,
				"link" 			=> '',//'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> '',
				"video"			=> '',//$value->desc_video_inv,
				"related"		=> array(),//$value->related,
				"link_shop"		=> array(),//$value->related,
				"collection_images"=>$collection,
				"price" 		=> $price,
				"sell_price"	=> $sell_price,
				"price_label"	=> $sell_price,
				"label"			=> '',
				"stock"			=> @$value->stock_var,
				"variant"		=> $id_var,			
				"inventory"		=> $id_var,
				"is_package"    => 1,
				"sku"			=> $id_var,
				"variant_option"=> $variant_option,
				"order_status"=> $order_status,
				"order_status_style"=> $order_status_style
			);

		
		$return = array(
			"data"=>$result,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }

	public function detail2(Request $request,$id_var)
    {
		$value = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->with('images.collection')
					->with('discount.parents')
					->with('related')
					->with('specification.lists')
					->take($this->limit)
					->skip($this->start)
					->where('id_var',$id_var)
					//->where('inv_var',$id_inv)
					->first(); 
		$result = array();
		//foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.'); 
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us"; 
			} 
			$collection = array();
			foreach($value->images as $image){
				$collection[]=array(
					"label"=> $image->collection[0]->label_img,
					"image"=>'inventory_ecommerce/'.$value->inv_var."/".$image->collection[0]->src_img
				);
			} 
			$vo = DB::table('variant_ec')->where('inv_var',$value->inv_var)->get();
			$variant_option = array();
			foreach ($vo as $k => $v) {
				$d = $v;
				$d->name = $value->product->name_inv." ".$v->attr1_var." ".$v->attr2_var." ".$v->attr3_var;
				$variant_option[] = $d;
			}

			$order_status = '';
			$order_status_style = '';
			if($value->preorder_inv==0){ 
				if($value->stock_var>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value->preorder_inv==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 

			$result = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"deskripsi"		=> $value->desc_inv,
				"box"			=> $value->desc_box_inv,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"video"			=> $value->desc_video_inv,
				"related"		=> $value->related,
				"collection_images"=>$collection,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"variant_option"=> $variant_option,
				"order_status"=> $order_status,
				"order_status_style"=> $order_status_style
			);
		//}
		
		$return = array(
			"data"=>$result,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }

    public function point_reward(Request $request)
    {
        $input = $request->input();
        $start = isset($input['start'])?$input['start']:0;
        $limit = isset($input['limit'])?$input['limit']:21;

        
    }



}
