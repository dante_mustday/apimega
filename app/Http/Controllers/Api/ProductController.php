<?php

namespace App\Http\Controllers\Api;

use App\Product;
use App\Discount;
use App\Variants;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class ProductController extends ApiController
{
	public function __construct()
	{
		parent::__construct();
	}
    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
	public function index(Request $request)
    {
		$result = Product::with('variants.images.collection')
			->with('related')
			->take(5)->get();
		$return = array(
			"data"=>$result,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
	}
	 
	 
    public function lists(Request $request)
    {
		$total = 0;	
    	$input = $request->input();
    	$tags = isset($input['tags']) && $input['tags']!=''?explode(",",$input['tags']):'';
    	$brand = isset($input['brand']) && $input['brand']!=''?explode(",",$input['brand']):'';
    	$sortBy = isset($input['sortBy']) && $input['sortBy']!=''?$input['sortBy']:'name_inv';
    	$order = isset($input['order']) && $input['order']!=''?$input['order']:'asc';
    	$q = isset($input['q']) && $input['q']!=''?$input['q']:'';

    	if($q!=''){
    		$q= "(searchname_inv LIKE '%".$q."%' or inventory_ec.name_inv LIKE '%".$q."%' or category LIKE '%".$q."%' or subcategory LIKE '%".$q."%' or subsubcategory LIKE '%".$q."%'  or brand_ec.label_brand LIKE '%".$q."%' )";
    	}
    	switch ($sortBy) {
    		case 'Nama':
    			$sortBy ='name_inv';
    			break;
    		case 'Produk Terpopuler':
    			$sortBy ='seq_var';
    			break;
    		case 'Produk terbaru':
    			$sortBy ='created_time_inv';
    			break;
    		case 'Stok':
    			$sortBy ='stock_var';
    			break;
    		case 'Harga Tertinggi':
    			$sortBy ='price_var';
    			break;
    		case 'Harga Terendah':
    			$sortBy ='price_var';
    			break;
    		default:
    			$sortBy = 'seq_var';
    			break;
    	}

		if(!isset($_GET['category_filter']) && !isset($_GET['cat_type'])){

				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('inventory_ec.removed_inv',0)
						->where('inventory_ec.status_inv',1)
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->take($this->limit)
						->skip($this->start);
						if($tags!=''){
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($q!=''){
							$data->whereRaw("  ($q) ");
						}
						$data->orderBy($sortBy,$order);
				$data	=$data->get();

				$total = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv');
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}
						}
						if($q!=''){
							$total->whereRaw("  ($q) ");
						}
				$total	=$total->count();
		}else{
			if($_GET['cat_type']=="cat"){
				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('allcategory_ec.category_id',$_GET['category_filter'])
						->where('inventory_ec.removed_inv',0)
						->where('inventory_ec.status_inv',1)
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->take($this->limit)
						->skip($this->start);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						$data->orderBy($sortBy,$order);
				$data	=$data->get();

				$total = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->where('allcategory_ec.category_id',$_GET['category_filter']);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
				$total	=$total->count();
			}else if($_GET['cat_type']=="sub"){
				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('allcategory_ec.subcategory_id',$_GET['category_filter'])
						->where('inventory_ec.removed_inv',0)
						->where('inventory_ec.status_inv',1)
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->take($this->limit)
						->skip($this->start);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						$data->orderBy($sortBy,$order);
				$data	=$data->get();
				$total = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->where('allcategory_ec.subcategory_id',$_GET['category_filter']);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
				$total	=$total->count();
			}else{
				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('allcategory_ec.subsubcategory_id',$_GET['category_filter'])
						->where('inventory_ec.removed_inv',0)
						->where('inventory_ec.status_inv',1)
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->take($this->limit)
						->skip($this->start);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						$data->orderBy($sortBy,$order);
				$data	=$data->get();
				$total = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->where('allcategory_ec.subsubcategory_id',$_GET['category_filter']);
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
				$total	=$total->count();
			}
		}
		$result = array();
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){

				$pd=DB::table("discount_detail_ec")
					->join('discount_ec','discount_ec.id_disc','discount_detail_ec.disc_dd')
					->where('status_disc',1)
					->where('removed_disc',0)
					->whereRaw('discount_ec.end_disc>now()')
					->where('var_dd',$value->id_var)->first();
				$price_disc = isset($pd->price_dd)?$pd->price_dd:0;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$order_status = '';
			$order_status_style = '';

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value['preorder_inv']==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 

			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"order_status"			=> $order_status,
				"order_status_style"	=> $order_status_style
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function hot(Request $request)
    {
		$total = 0;
		if(!isset($_GET['category_filter']) && !isset($_GET['cat_type'])){
				$data = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->with('discount.parents')
					->take($this->limit)
					->skip($this->start)
					->where('label_inv','like','%hot%')
					->orderBy('seq_var','DESC')
					->get();
		}else{
				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('allcategory_ec.id',$_GET['category_filter'])
						->where('label_inv','like','%hot%')
						->take($this->limit)
						->skip($this->start)
						->get();
			
		}
		$total	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('label_inv','like','%hot%')->count();
		$result = array();
		
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$order_status = '';
			$order_status_style = '';

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value['preorder_inv']==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 
			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"order_status"			=> @$order_status,
				"order_status_style"			=> @$order_status_style
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function news(Request $request)
    {
		$total = 0;
		$data =  Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
			->where('label_inv','like','%new%')
			->take(6)
			->get();
		$total	= 6;
		$result = array();
		
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}

			$order_status = '';
			$order_status_style = '';

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value['preorder_inv']==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 
			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"order_status"	=> $order_status,
				"order_status_style"=> @$order_status_style
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function promo(Request $request)
    {
		$total = 0;
		if(!isset($_GET['category_filter']) && !isset($_GET['cat_type'])){
				
			$data 	= Discount::join('discount_detail_ec','discount_ec.id_disc','=','discount_detail_ec.disc_dd')
					->join('variant_ec', 'variant_ec.id_var', '=', 'discount_detail_ec.var_dd')
					->join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->where('discount_ec.status_disc',1)
					->where('discount_ec.removed_disc',0)
					->where('discount_ec.end_disc','>',DB::raw('now()'))
					->take(6)
					->orderBy('discount_ec.id_disc',DB::raw('RAND()'))
					->get();
		}else{

			$data 	= Discount::join('discount_detail_ec','discount_ec.id_disc','=','discount_detail_ec.disc_dd')
					->join('variant_ec', 'variant_ec.id_var', '=', 'discount_detail_ec.var_dd')
					->join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->where('discount_ec.status_disc',1)
					->where('discount_ec.removed_disc',0)
					->where('discount_ec.end_disc','>',DB::raw('now()'))
					->take(6)
					->orderBy('discount_ec.id_disc',DB::raw('RAND()'))
					->get();
			
		}
		$total	=  Discount::join('discount_detail_ec','discount_ec.id_disc','=','discount_detail_ec.disc_dd')
				->join('variant_ec', 'variant_ec.id_var', '=', 'discount_detail_ec.var_dd')
				->join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
				->where('discount_ec.status_disc',1)
				->where('discount_ec.removed_disc',0)
				->where('discount_ec.end_disc','>',DB::raw('now()'))
				->take(6)
				->orderBy('discount_ec.id_disc',DB::raw('RAND()'))
				->count();
		$result = array();
		
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->price_dd){
				$price_disc=@$value->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:green'>
						IN STOCK
					</span>";
			  }else{
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:red'>
						BY ORDER
					</span>";
			 } } else if($value['preorder']==1){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Preorder
					</span>";
			 }else{ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Discontinue
					</span>";
			} 

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value['preorder_inv']==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 
			$result[] = array(
				"name"			=> @$value->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> '/product/'.@$value->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv!='' && @$value->product->label_inv!=null?@$value->product->label_inv:'',
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"stock_label"   => $stock_label,
				"end_date" 		=> $value->end_disc,
				"order_status" 		=> $order_status,
				"order_status_style" 		=> $order_status_style
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function selloff(Request $request)
    {
		$total = 0;
		if(!isset($_GET['category_filter']) && !isset($_GET['cat_type'])){
				$data = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->with('discount.parents')
					->take($this->limit)
					->skip($this->start)
					->where('label_inv','like','%sell off%')
					->orderBy('seq_var','DESC')
					->get();
		}else{
				$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('allcategory_ec.id',$_GET['category_filter'])
						->where('label_inv','like','%sell off%')
						->take($this->limit)
						->skip($this->start)
						->get();
			
		}
		$total	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
						join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')
						->where('label_inv','like','%sell off%')->count();
		$result = array();
		
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
	
	public function detail(Request $request,$id_var)
    {

		$value = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->with('images.collection')
					->with('discount.parents')
					->with('related')
					->with('specification.lists')
					->take($this->limit)
					->skip($this->start)
					->where('id_var',$id_var)
					//->where('inv_var',$id_inv)
					->first();
		$result = array();
		//foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty($value->price_var)) ? $value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if($value->discount){
				$price_disc=$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if($value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$collection = array();
			foreach($value->images as $image){
				$collection[]=array(
					"label"=> $image->collection[0]->label_img,
					"image"=>'inventory_ecommerce/'.$value->inv_var."/".$image->collection[0]->src_img
				);
			} 
			$vo = DB::table('variant_ec')->where('inv_var',$value->inv_var)->get();
			$variant_option = array();

			foreach ($vo as $k => $v) {
				$d = $v;
				$d->name = $value->product->name_inv." ".$v->attr1_var." ".$v->attr2_var." ".$v->attr3_var;
				$variant_option[] = $d;
			}

			$order_status = '';
			$order_status_style = '';
			if($value->preorder_inv==0){ 
				if($value->stock_var>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value->preorder_inv==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 

			//link shop
			$link_shop = array();
			if($value->link_shop1!='' && $value->image_shop1!=''){
				$link_shop[] = array('link'=>$value->link_shop1,"src"=>$value->image_shop1);
			}
			if($value->link_shop2!='' && $value->image_shop2!=''){
				$link_shop[] = array('link'=>$value->link_shop2,"src"=>$value->image_shop2);
			}
			if($value->link_shop3!='' && $value->image_shop3!=''){
				$link_shop[] = array('link'=>$value->link_shop3,"src"=>$value->image_shop3);
			}
			if($value->link_shop4!='' && $value->image_shop4!=''){
				$link_shop[] = array('link'=>$value->link_shop4,"src"=>$value->image_shop4);
			}

			//spec

			$cat = DB::table('allcategory_ec')->where("id",$value->product->category_inv)->first();
			
			$spec = DB::table('specification_ec')
					->select('id_spec','name_spec');
			if($cat->subsubcategory_id!=""){
				$spec->where('subsubcat_id',$cat->subsubcategory_id);
			}else if($cat->subcategory_id!=""){
				$spec->where('subcat_id',$cat->subcategory_id);
			}else{
				$spec->where('cat_id',$cat->category_id);
			}
			$spec->where("status_spec",1)->where("removed_spec",0);
			$specGroup = $spec->orderBy("order_spec","asc")->get();
			$specification = '';
			$specification = '
				<table width="100%" id="spec">
			';
			foreach ($specGroup as $key => $v) {
					$spec = $v->id_spec;
					$spec_val = DB::table('inventory_specification_ec')->select("value_is","name_spec")
					->where('inv_is',$value->product->id_inv)
					->where('spec_is',$spec)
					->first();

					$sv = (!empty($spec_val)) ? $spec_val->value_is : "";
					$specification .= '
						<tr>
							<td width="30%"  >'.$v->name_spec.':</td>
							<td  width="70%"  >'.$sv.'</td>
						<tr>
					';

					//$specification[$key]['id_spec'] = $spec;
					//$specification[$key]['name_spec'] = $v->name_spec;
					//$specification[$key]['value_spec'] = (!empty($spec_val)) ? $spec_val->value_is : "";
			}

			$specification .= '
				</table>
			';

			$result = array(
				"realname"		=> @$value->product->name_inv,
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"deskripsi"		=> $value->desc_inv,
				"box"			=> $value->desc_box_inv,
				//"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"link" 			=> @$value->product->permalink_inv, 
				"image" 		=> $img,
				"video"			=> $value->desc_video_inv,
				"related"		=> $value->related,
				"collection_images"=>$collection,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"variant_option"=> $variant_option,
				"order_status"=> $order_status,
				"link_shop"=> $link_shop,
				"specification"=>$specification,
				"order_status_style"=> $order_status_style,
				"weight"=>$value->unit_shipping_var
			);
		//}
		if(isset($_GET['use_point']) && $_GET['use_point']==1){
			$result['use_point'] = 1;
			$result['price_point'] =$value->point_spend_var;
		}
		$return = array(
			"data"=>$result,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }

    public function point_reward(Request $request)
    {
        $input = $request->input();
        $start = isset($input['start'])?$input['start']:0;
        $limit = isset($input['limit'])?$input['limit']:21;

        
    }
	
	public function homenew(Request $request)
    {
		$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
			->where('label_inv','like','%new%')
			->take(6)
			->get();


		$result = array();
		
		foreach ($data as $value) {
			$img = '/backend/uploads/inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->product->preorder_inv==0){ 
				if(@$value->product->type_inv>=1 and $value->stock_var>0){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:green'>
						IN STOCK
					</span>";
			  }else{
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:red'>
						BY ORDER
					</span>";
			 } } else if(@$value->product->preorder_inv==1){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Preorder
					</span>";
			 }else{ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Discontinue
					</span>";
			} 
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> '/product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"stock_label"   => $stock_label
			);
		}
		
		$data['new'] = $result;
		
		return view('load-new-home', $data);
    }
	
	public function homepromo(Request $request)
    {

		$data 	= Discount::join('discount_detail_ec','discount_ec.id_disc','=','discount_detail_ec.disc_dd')
				->join('variant_ec', 'variant_ec.id_var', '=', 'discount_detail_ec.var_dd')
				->join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
				->where('discount_ec.status_disc',1)
				->where('discount_ec.removed_disc',0)
				->where('discount_ec.end_disc','>',DB::raw('now()'))
				->take(6)
				->orderBy('discount_ec.id_disc',DB::raw('RAND()'))
				->get();
		//dd($data);
		$result = array();
		
		foreach ($data as $value) {
			$img = '/backend/uploads/inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_label="";
			$price_disc=@$value->price_dd;
			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1 and $value->stock_var>0){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:green'>
						IN STOCK
					</span>";
			  }else{
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:red'>
						BY ORDER
					</span>";
			 } } else if($value['preorder']==1){ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Preorder
					</span>";
			 }else{ 
					$stock_label = "<span  style='margin:0px 3px;font-size: 10px;color:orange'>
						Discontinue
					</span>";
			} 

			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$x='line-through';
					$price_label = "<span style='width:100%;color:#767573;font-weight:400;font-size:9pt;text-decoration:$x'>Rp. ".number_format($price,0,',','.')."</span><br/>";
					if($value['status_disc']==1){
					$price_label .= "<span style='color:red'>Rp. ".number_format($price_disc)."</span>";
					}
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			
			$result[] = array(
				"name"			=> @$value->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> '/product/'.@$value->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"stock_label"   => $stock_label,
				"end_date" 		=> $value->end_disc
			);
		}
		
		$data['promo'] = $result;
		return view('load-promo-home', $data);
    }



}
