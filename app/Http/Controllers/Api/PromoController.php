<?php

namespace App\Http\Controllers\Api;

use App\Banner;
use Carbon\Carbon;
use App\Product;
use App\Discount;
use App\Variants;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class PromoController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request)
    {
        $data = Discount::where('status_disc',1)
			->where('removed_disc',0)
			->where('discount_ec.end_disc','>=',date('Y-m-d'))
			->orderBy('seq_disc','ASC')->get();

		$r=array();
		foreach ($data as $k => $v) {
			$desc = '';
				$desc .="<br>".date('d/m/Y',strtotime($v->created_time_disc))." S/d ".date('d/m/Y',strtotime($v->end_disc));


			$v->desc = $desc;
			$v->src = "discount_ecommerce/".$v->img_disc;
			$r[]= $v;
		}
		$response = array(
			"data"=>$r,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

    public function listsProduct(Request $request)
    {
		$total = 0;	
    	$input = $request->input();
    	$tags = isset($input['tags']) && $input['tags']!=''?explode(",",$input['tags']):'';
    	$brand = isset($input['brand']) && $input['brand']!=''?explode(",",$input['brand']):'';
    	$sortBy = isset($input['sortBy']) && $input['sortBy']!=''?$input['sortBy']:'name_inv';
    	$order = isset($input['order']) && $input['order']!=''?$input['order']:'asc';
    	$category_filter = isset($input['category_filter']) && $input['category_filter']!=''?$input['category_filter']:'';
    	$q = isset($input['q']) && $input['q']!=''?$input['q']:'';

    	if($q!=''){
    		$q= "(searchname_inv LIKE '%".$q."%' or inventory_ec.name_inv LIKE '%".$q."%' or category LIKE '%".$q."%' or subcategory LIKE '%".$q."%' or subsubcategory LIKE '%".$q."%'  or brand_ec.label_brand LIKE '%".$q."%' )";
    	}
    	switch ($sortBy) {
    		case 'Nama':
    			$sortBy ='inventory_ec.name_inv';
    			break;
    		case 'Produk Terpopuler':
    			$sortBy ='variant_ec.seq_var';
    			break;
    		case 'Produk terbaru':
    			$sortBy ='inventory_ec.created_time_inv';
    			break;
    		case 'Stok':
    			$sortBy ='variant_ec.stock_var';
    			break;
    		case 'Harga Tertinggi':
    			$sortBy ='variant_ec.price_var';
    			break;
    		case 'Harga Terendah':
    			$sortBy ='variant_ec.price_var';
    			break;
    		default:
    			$sortBy = 'variant_ec.seq_var';
    			break;
    	}

		$data 	= Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
				join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')->
				join('allproduct_ec', 'allproduct_ec.id_var', '=', 'variant_ec.id_var')->
				join('discount_detail_ec', 'discount_detail_ec.var_dd', '=', 'variant_ec.id_var')->
				join('discount_ec', 'discount_detail_ec.disc_dd', '=', 'discount_ec.id_disc')
				->where('discount_detail_ec.disc_dd',$category_filter)
				//->where('allcategory_ec.category_id',10)
				->where('removed_disc',0)
				->where('status_disc',1)
				->whereDate('discount_ec.end_disc',">=",date('Y-m-d'))
				->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
				->take($this->limit)
				->skip($this->start);
				if($tags!=''){	
					$t = array();
					foreach ($tags as $k => $v) {
						$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
					}
					$t = implode(" || ",$t);
					if($t!=''){
						$data->whereRaw("  ($t) ");
					}

				}
				if($brand!=''){
					$data->whereRaw(" brand_ec.label_brand IN ('$brand') ");	
				}
				$data->orderBy($sortBy,$order);
		$data	=$data->get();

		$total =  Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')->
				join('allcategory_ec', 'allcategory_ec.id', '=', 'inventory_ec.category_inv')->
				join('allproduct_ec', 'allproduct_ec.id_var', '=', 'variant_ec.id_var')->
				join('discount_detail_ec', 'discount_detail_ec.var_dd', '=', 'variant_ec.id_var')->
				join('discount_ec', 'discount_detail_ec.disc_dd', '=', 'discount_ec.id_disc')
				->where('discount_detail_ec.disc_dd',$category_filter)
				//->where('allcategory_ec.category_id',10)
				->where('removed_disc',0)
				->where('status_disc',1)
				->whereDate('discount_ec.end_disc',">=",date('Y-m-d'))
				->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
				;
				if($tags!=''){	
					$t = array();
					foreach ($tags as $k => $v) {
						$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
					}
					$t = implode(" || ",$t);
					if($t!=''){
						$total->whereRaw("  ($t) ");
					}

				}
				if($brand!=''){
					$total->whereRaw(" brand_ec.label_brand IN ('$brand') ");	
				}
		$total	=$total->count();

		$result = array();
		foreach ($data as $value) {

			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$order_status = '';
			$order_status_style = '';

			if($value['preorder_inv']==0){ 
				if($value['stock_var']>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value['preorder_inv']==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 

			$result[] = array(
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"order_status"			=> $order_status,
				"order_status_style"	=> $order_status_style
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }

    public function detail($id,Request $request)
    {
        $v = DB::table("event_ec")
			->where('id',$id)->first();


		if($v->type==1){
				$date = "Pukul : ".$v->time." WIB";
				$date .="<br>".date('d/m/Y',strtotime($v->start_date))." S/d ".date('d/m/Y',strtotime($v->end_date));
		}else{
				$date = date('d/m/Y', strtotime($v->date))." | Pukul : ".$v->time." WIB";
		}
		$desc = "
			<table width='100%'>
				<tr>
					<td align='center'>Tempat:</td>
				</tr>
				<tr>
					<td  align='center'><b>".$v->place."</b></td>
				</tr>
				<tr>
					<td  align='center'>&nbsp;</td>
				</tr>
				<tr>
					<td  align='center'>Tanggal:</td>
				</tr>
				<tr  align='center'>
					<td><b>".$date."</b></td>
				</tr>
			</table>
			<br>
			<div>
				$v->text
			</div>
		";

		$v->desc = $desc;
		$v->src = "event_ecommerce/".$v->thumbnail;
		$response = array(
			"data"=>$v,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }


	public function requestItem(Request $request)
    {

        try {
        	$response = array();
        	$data = $request->json()->all();

            $validation = Validator::make($request->json()->all(), [
				'fullname'=>'required',
                'email' => 'required',
                'telephone' => 'required',
                'barang' => 'required',
            ]);
				
            if ($validation->fails()) {
				$response = array(
					"data"=>$validation->errors(),
					"status"=>"400",
					"error"=>true,
					"message"=>"validation error"
				);
				return $response;
            }


            $auth = DB::table('request_item_ec')->insert($request->json()->all());
            // send email
			
			$response = array(
					"data"=>$auth,
					"status"=>"ok",
				  	"error"=>false,
					"message"=>"request item berhasil terkirim"
			);
			
			return $response;

        } catch (Exception $e) {
            //return $this->error($e->getMessage(), $e->getCode());

			$response = array(
				"data"=>array(),
				"status"=>"error",
			  	"error"=>true,
				"message"=>$this->error($e->getMessage(), $e->getCode())
			);
			return $response;
        }
    }

    public function about()
    {
		$page = DB::table('page_ec')->where("permalink_page",'tentang-megakamera')->where('status_page',1)->get();
		return array('data'=>isset($page[0])?$page[0]:array());
    }


}
