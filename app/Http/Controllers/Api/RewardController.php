<?php

namespace App\Http\Controllers\Api;

use App\Paket;
use App\Variants;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class RewardController extends ApiController
{
	public function __construct()
	{
		parent::__construct();
	}


	 
    public function lists(Request $request)
    {
		$total = 0;	
    	$input = $request->input();
    	$tags = isset($input['tags']) && $input['tags']!=''?explode(",",$input['tags']):'';
    	$brand = isset($input['brand']) && $input['brand']!=''?explode(",",$input['brand']):'';
    	$sortBy = isset($input['sortBy']) && $input['sortBy']!=''?$input['sortBy']:'name_inv';
    	$order = isset($input['order']) && $input['order']!=''?$input['order']:'asc';
    	$q = isset($input['q']) && $input['q']!=''?$input['q']:'';

    	if($q!=''){
    		$q= "(searchname_inv LIKE '%".$q."%' or inventory_ec.name_inv LIKE '%".$q."%' or category LIKE '%".$q."%' or subcategory LIKE '%".$q."%' or subsubcategory LIKE '%".$q."%'  or brand_ec.label_brand LIKE '%".$q."%' )";
    	}
    	switch ($sortBy) {
    		case 'Nama':
    			$sortBy ='name_inv';
    			break;
    		case 'Produk Terpopuler':
    			$sortBy ='seq_var';
    			break;
    		case 'Produk terbaru':
    			$sortBy ='created_time_inv';
    			break;
    		case 'Stok':
    			$sortBy ='stock_var';
    			break;
    		case 'Harga Tertinggi':
    			$sortBy ='price_var';
    			break;
    		case 'Harga Terendah':
    			$sortBy ='price_var';
    			break;
    		default:
    			$sortBy = 'seq_var';
    			break;
    	}

		

		$data 	= DB::table("allproduct_point_ec")->leftjoin('inventory_ec', 'inventory_ec.id_inv', '=', 'allproduct_point_ec.id_inv')
						->where('inventory_ec.removed_inv',0)
						->where('inventory_ec.status_inv',1)
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv')
						->take($this->limit)
						->skip($this->start);
						if($tags!=''){
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$data->whereRaw("  ($t) ");
							}

						}
						if($q!=''){
							$data->whereRaw("  ($q) ");
						}
						$data->orderBy($sortBy,$order);
				$data	=$data->get();

				$total = DB::table("allproduct_point_ec")->join('inventory_ec', 'inventory_ec.id_inv', '=', 'allproduct_point_ec.id_inv')
						->leftjoin("brand_ec",'brand_ec.id_brand','inventory_ec.brand_inv');
						if($tags!=''){	
							$t = array();
							foreach ($tags as $k => $v) {
								$t[] = " inventory_ec.label_inv LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}

						}
						if($brand!=''){
							$t = array();
							foreach ($brand as $k => $v) {
								$t[] = " brand_ec.label_brand LIKE '%$v%' ";
							}
							$t = implode(" || ",$t);
							if($t!=''){
								$total->whereRaw("  ($t) ");
							}
						}
						if($q!=''){
							$total->whereRaw("  ($q) ");
						}
				$total	=$total->count();
		
		$result = array();

		foreach ($data as $value) {

			$img = 'inventory_ecommerce/'.$value->id_inv."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$order_status = '';
			$order_status_style = '';

			if($value->preorder_inv==0){ 
				if($value->stock_var>=1){ 
					$order_status = 'In Stock';
					$order_status_style ='green';
				}else{ 
					$order_status = 'By Order';
					$order_status_style ='red';
				} 
			} else if($value->preorder_inv==1){ 
				$order_status = 'Preorder';
				$order_status_style ='orange';
			}else{ 
				$order_status = 'Discontinue';
				$order_status_style ='orange';
			} 

			$result[] = array(
				"name"			=> @$value->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> "Use Point",
				"label"			=> @$value->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->id_inv,
				"sku"			=> @$value->sku_var,
				"order_status"			=> $order_status,
				"order_status_style"	=> $order_status_style,
				"use_point"	=> 1
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $return;
    }
    



}
