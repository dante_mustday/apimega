<?php

namespace App\Http\Controllers\Api;

use App\City;
use App\District;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;

class SettingController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function allCity(Request $request)
    {
        $result = City::orderBy('kabupaten','ASC')->get();
		$response = array(
			"data"=>$result,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function searchCity(Request $request)
    {
		$data = $request->json()->all();
        $result = City::where('kabupaten', 'like', '%'.$data['param'].'%')->orderBy('kabupaten','ASC')->get();
		$response = array(
			"data"=>$result,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function districtByCity(Request $request,$id)
    {
        $result = District::where('id_kabupaten',$id)->orderBy('kabupaten','ASC')->get();
		$response = array(
			"data"=>$result,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function settingCompany(Request $request)
    {
        $data = DB::table("global_param_ec")->get();
		$result = array();
		foreach($data as $row){
			if($row->type_gp != "image"){
			$result[$row->name_gp] = $row->value_gp;
			}else{
				$result[$row->name_gp] = "backend/uploads/".$row->value_gp;
			}
		}
		$response = array(
			"data"=>$result,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

    public function registerId(Request $request)
    {
        $app_id = 'dailycardID';
        $signature = $request->header('Signature');  // for profile
        $input = $request->input(); // cardsArrya,

        //encode appid 
        $status='error';
        $msg = 'tidak valid';
        $data = array();
        $check_data = base64_decode($signature);
        if (strpos($check_data, $app_id) !== false) {
            $status='ok';
            $msg = 'valid';
            //if not error get data
            $data = array('setting_data'=>array()); // get setting dta to save local store

        }
        $return['status']=$status;
        $return['msg']=$msg;
        $return['data']=$data;
        return $return;
    }


}
