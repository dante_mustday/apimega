<?php

namespace App\Http\Controllers\Api;

use App\Slider;
use Carbon\Carbon;
use DB;
use App\Discount;
use Exception;
use Illuminate\Http\Request;

class SliderController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request)
    {
        $slider = Slider::select(DB::raw("CONCAT('slider_ecommerce/',photo_slider) AS image"),'label_slider as label','link_slider as link')->where('removed_slider',0)->where('status_slider',1)->orderBy('seq_slider','ASC')->get();
        $response = array(
			"data"=>$slider,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

    public function listswithpromo(Request $request)
    {
        $promo     = Discount::where('discount_ec.status_disc',1)
                ->where('discount_ec.removed_disc',0)
                ->where('discount_ec.start_disc','<=',DB::raw('now()'))
                ->where('discount_ec.end_disc','>=',DB::raw('now()'))
                ->orderBy('discount_ec.id_disc',DB::raw('RAND()'))
                ->get();
        $slider = Slider::select(DB::raw("CONCAT('slider_ecommerce/',photo_slider) AS image"),'label_slider as label','link_slider as link')->where('removed_slider',0)->where('status_slider',1)->orderBy('seq_slider','ASC')->get();

        $slidehome = array();
        foreach($promo as $row){
            $slidehome[] = array(
                "image"=>'discount_ecommerce/'.$row->img_disc,
                "lable"=>$row->name_disc,
                "link"=>"/promo/".$row->code_disc
            );
        }

        foreach($slider as $row){
            $slidehome[] = array(
                "image"=>$row->image,
                "lable"=>$row->label,
                "link"=>$row->link
            );
        }

        $response = array(
            "data"=>$slidehome,
            "status"=>"ok",
            "message"=>"success"
        );
        return $response;
    }


}
