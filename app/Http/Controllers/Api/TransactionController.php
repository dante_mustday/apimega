<?php

namespace App\Http\Controllers\Api;

use App\Transaction;
use App\Variants;
use App\TransactionDetail;
use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;

class TransactionController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
    public function lists(Request $request,$customer,$status)
    {
		//$status = $request->input('status');
		//$customer = $request->input('customer');
		if($status=="Rejected" || $status=="Canceled"){
			$transaction = Transaction::where('customer_trans',$customer)->whereIn('status_trans', array("Rejected","Canceled"))->orderBy('created_time_trans','desc')->limit('30')->get();
		}else if($status=="Verified" || $status=="Confirmed"){
			$transaction = Transaction::where('customer_trans',$customer)->whereIn('status_trans', array("Verified","Confirmed","New Order"))->orderBy('created_time_trans','desc')->limit('30')->get();
		}else{
			$transaction = Transaction::where('customer_trans',$customer)->whereIn('status_trans', array("Shipped"))->limit('30')->orderBy('created_time_trans','desc')->get();
		}
		
		$r=array();
		foreach ($transaction as $k => $v){		
			$v->total_item = count(TransactionDetail::where('trans_dt',$v->id_trans)->get());
			$r[] = $v;
		}
		$response = array(
			"data"=>$r,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
	
	public function detail(Request $request,$invoice)
    {
		$transaction = Transaction::with('details')->where('invoice_number',$invoice)->first();
		
		$response = array(
			"data"=>$transaction,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }

    public function bank()
    {
        $bank = DB::table('bank_ec')->where("status_bank",1)->get();

		$response = array(
			"data"=>$bank,
			"status"=>"ok",
			"message"=>"success"
		);
        return $response;
    }
    public function paymentConfirmation(Request $request)
    {
    	$input = $request->json()->all();
    	if(!isset($input['no_transaksi'])){
			$response = array(
				"status"=>"error",
				"message"=>"no transaksi tidak ditemukan"
			);
	        return $response;
    	}

    	if($input['no_transaksi'] == ''){
			$response = array(
				"status"=>"error",
				"message"=>"no transaksi tidak ditemukan"
			);
	        return $response;
    	}
    	$chk = Transaction::where('invoice_number',$input['no_transaksi'])->first();
    	if(!isset($chk->invoice_number)){
			$response = array(
				"status"=>"error",
				"message"=>"no transaksi tidak ditemukan"
			);
	        return $response;
    	}
    	$input_data = array(
				'trans_pc'    => $chk->id_trans,
                'invoice_number'    => $input['no_transaksi'],
				'total_pc'    =>  $chk->grand_total_trans,//$post['jumlah_transfer'],
				'date_pc'    => date("Y-m-d",strtotime($input['tgl_transfer'])),
				'bank_pc'    => $input['rekening_tujuan'],
				'acc_no_pc'    => $input['no_rekening'],
				'acc_name_pc'    => $input['atas_nama']
		);
		DB::table('payment_confirm_ec')->insert($input_data);
			$response = array(
				"data"=>array(),
				"status"=>"ok",
				"message"=>"berhasil, transaksi akan di proses"
		);
	    return $response;
    }

    public function getOnWishList(Request $request)
    {
    	$input = $request->json()->all();
    	if(!isset($input['id_cust']) || !isset($input['inventory']) || !isset($input['variant'])){
			$response = array(
				"error"=>true,
				"status"=>"error",
				"message"=>" tidak ditemukan"
			);
	        return $response;
    	}

    	$chk = DB::table('wishlist_ec')->where('cust_ws',$input['id_cust'])->where('inv_ws',$input['variant'])->first();
    	if(isset($chk->cust_ws)){
			$response = array(
				"error"=>false,
				"data"=>array("on_wish_list"=>true),
				"status"=>"error",
				"message"=>"success"
			);

    	}else{

			$response = array(
				"error"=>false,
				"data"=>array("on_wish_list"=>false),
				"status"=>"error",
				"message"=>"success"
			);
    	}
    	return $response;

    }


    public function setOnWishlist(Request $request)
    {
    	$input = $request->json()->all();
    	if(!isset($input['id_cust']) || !isset($input['inventory']) || !isset($input['variant'])){
			$response = array(
				"error"=>true,
				"status"=>"error",
				"message"=>" tidak ditemukan"
			);
	        return $response;
    	}

    	$chk = DB::table('wishlist_ec')->where('cust_ws',$input['id_cust'])->where('inv_ws',$input['variant'])->first();
    	if(isset($chk->cust_ws)){
    		DB::table('wishlist_ec')->where('cust_ws',$input['id_cust'])->where('inv_ws',$input['variant'])->delete();
			$response = array(
				"error"=>false,
				"data"=>array("on_wish_list"=>false),
				"status"=>"error",
				"message"=>"success"
			);
    	}else{
	    	$input_data = array(
	                'cust_ws'    => $input['id_cust'],
					'inv_ws'    => $input['variant'],
			);
			DB::table('wishlist_ec')->insert($input_data);
			$response = array(
				"error"=>false,
				"data"=>array("on_wish_list"=>true),
				"status"=>"error",
				"message"=>"success"
			);
    	}
    	return $response;

    }

    public function wishlist($id_cust)
    {
    	$r = DB::table('wishlist_ec')->where('cust_ws',$id_cust)->orderBy('id_ws','desc')->limit(21)->get();

    	$id = array();
    	foreach ($r as $k => $v) {
    		$id[] = $v->inv_ws;
    	}

    	$data = $this->getProductWishlist($id);

		$response = array(
				"error"=>false,
				"data"=>$data,
				"status"=>"error",
				"message"=>"success"
		);
    	return $response;

    }

	public function getProductWishlist($id)
    {
		$total = 0;
		$data = Variants::join('inventory_ec', 'inventory_ec.id_inv', '=', 'variant_ec.inv_var')
					->with('discount.parents')
					->take(21)
					->skip(0)
					->whereIn('id_var',$id)
					->get();

		$result = array();
		
		foreach ($data as $value) {
			$img = 'inventory_ecommerce/'.$value->inv_var."/".$value->src_img;
			$price = (!empty(@$value->price_var)) ? @$value->price_var : 0;
			$price_disc=0;
			$price_label="";
			if(@$value->discount){
				$price_disc=@$value->discount->price_dd;
			}
			if(@$value->type_inv==1){
				if(@$value->status_disc==1){
					$price_label="Rp. ".number_format($price_disc,0,',','.');
				}else{
					$price_label="Rp. ".number_format($price,0,',','.');
				}
			}else{
				$price_label="Call us";
			}
			$result[] = array(
				/*
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"image" 		=> $img,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var
				*/
				"realname"		=> @$value->product->name_inv,
				"name"			=> @$value->product->name_inv." ".$value->attr1_var." ".$value->attr2_var." ".$value->attr3_var,
				"deskripsi"		=> $value->desc_inv,
				"box"			=> $value->desc_box_inv,
				//"link" 			=> 'product/'.@$value->product->permalink_inv.'/'.@$value->id_var, 
				"link" 			=> @$value->product->permalink_inv, 
				"image" 		=> $img,
				"video"			=> $value->desc_video_inv,
				"related"		=> $value->related,
				"price" 		=> $price,
				"sell_price"	=> $price_disc,
				"price_label"	=> $price_label,
				"label"			=> @$value->product->label_inv,
				"stock"			=> @$value->stock_var,
				"variant"		=> @$value->id_var,			
				"inventory"		=> @$value->inv_var,
				"sku"			=> @$value->sku_var,
				"weight"=>$value->unit_shipping_var
			);
		}
		
		$return = array(
			"data"=>$result,
			"total"=>$total,
			"message"=>"Success get data",
			"status"=>"ok"
		);
        return $result;
    }


}
