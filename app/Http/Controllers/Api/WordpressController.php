<?php

namespace App\Http\Controllers\Api;

use Carbon\Carbon;
use DB;
use Exception;
use Illuminate\Http\Request; 

class WordpressController extends ApiController
{

    /**
     * @param Request $request
     * @return \Illuminate\Http\Response
     */
	 
	public function __construct()
    {
		/*header('Access-Control-Allow-Origin: http://megakamera.com');
		header('Access-Control-Allow-Methods: GET, PUT, POST, DELETE, OPTIONS');
		header('Access-Control-Max-Age: 1000');
		header('Access-Control-Allow-Headers: Content-Type, Authorization, X-Requested-With');*/
	}
	
    public function index(Request $request)
    {

		$curl = curl_init();
		$url= "https://public-api.wordpress.com/rest/v1/sites/megakamerablog.wordpress.com/posts/";
		curl_setopt ($curl, CURLOPT_URL,$url);
		curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
		curl_setopt($curl, CURLOPT_HTTPHEADER, array('Expect:'));
		if(!curl_exec($curl)){
			curl_close ($curl);
			$title="No Post";
			$url="#";
			$excerpt="Sorry No Post";
			$img="/backend/uploads/default.jpg";
			$date=date("Y-m-d");
			$data['articles'] = array();
		}else{
			$result = curl_exec ($curl);
			curl_close ($curl);
			$x=json_decode($result);
			$x=json_decode($result,true);
			$data['articles'] = $x['posts'];
		}

		return view('load-articles', $data);
	}

}
