<?php

namespace App\Http\Controllers\Web;

use Illuminate\Http\Request;
use Illuminate\Routing\Controller;
use Illuminate\Validation\ValidationException;
use DB;
use App\Productspecification as Invspec;
use App\Specificationnew as Spec;

class CoreController extends Controller
{
    /**
     * Send the error response.
     *
     * @param  string|\Exception $message
     * @param  int $httpCode
     * @return \Illuminate\Http\Response
     */
	 

    public function __construct() {
        DB::enableQueryLog();  
	}
	 
    public function index()
    { 
        $category = DB::table("specification_group_ec as sge")
		->join("subsubcategory_ec as se",'sge.id_sg', '=', 'se.specgroup_sscat')
		->join("specification_ec_wrong as spec","spec.group_spec",'=',"sge.id_sg") 
		//->join("inventory_specification_ec as ispec","ispec.spec_is",'=',"spec.id_spec")
		->where("se.removed_sscat","=",0)
		->get();
		dd($category);
		foreach($category as $cat){
			
			$check = Spec::where("subsubcat_id","=",$cat->id_sscat)->where("name_spec","=",$cat->name_spec)->get();
			if(count($check)==0){
				$spec = New Spec();
				$spec->name_spec = $cat->name_spec;
				$spec->subsubcat_id = $cat->id_sscat;
				$spec->order_spec = $cat->order_spec;
				$spec->status_spec = $cat->status_spec;
				$spec->removed_spec = $cat->removed_spec;
				$spec->save();
			}
		}
		echo "Sukses";
    }
	
	public function renderdata()
    {  
        $category = DB::table("inventory_ec as ie")
		->join("allcategory_ec as ae","ie.category_inv",'=',"ae.id")
		//->join("specification_ec_wrong as spec","spec.group_spec",'=',"sge.id_sg") 
		->join("specification_ec as spec",'ae.category_id', '=', 'spec.cat_id')
		//->join("inventory_specification_ec as ispec","ispec.inv_is",'=',"ie.id_inv")
		//->where("ispec.spec_is","=","spec.id_spec")
		->take(5000)  
		->skip(17000)
		//->select("spec.*","ae.*","ie.id_inv")
		//->groupBy("spec.id_spec")
		//->orderBy("ie.id_inv")
		->get();
		
		dd( $category);
		
		foreach($category as $cats){
			
			/*$data = DB::table("inventory_specification_ec as ispec")
			->join("specification_ec as spec","ispec.spec_is",'=',"spec.id_spec")
			->where("inv_is","=",$cats->id_inv)
			->where("id_spec","=",$cats->id_spec)
			->take(1000)
			->skip(0)
			->get();
			dd($data);
			foreach($data as $cat){*/
				
				$spec = New Invspec();
				$spec->inv_is 		= $cats->id_inv;
				$spec->spec_is 		= $cats->id_spec;
				$spec->spec_name 	= $cats->name_spec;
				//$spec->value_is 	= $cat->value_is;
				$spec->save();
			//}
		}
		echo "Sukses";
    }
	
	public function injectdata2()
    {  
	dd("A");
		$i=40000;
        $category = DB::table("inventory_specification_ec_copy")
		->take(5000)  
		->skip($i)
		->get();
		
		
		
		foreach($category as $cats){
			//print_r($cats);		 
			$data = DB::table("inventory_specification_ec as ispec")
			->join("specification_ecs as spec","ispec.spec_is",'=',"spec.id_spec")
			->where("inv_is","=",$cats->inv_is)
			->where("name_spec","=",$cats->name_spec)
			->first();
			
			//print_r($data);
			if($data){
			DB::table('inventory_specification_ec_copy')
				->where("inv_is","=",$cats->inv_is)
				->where("name_spec","=",$cats->name_spec)
				->update(['value_is' => $data->value_is]);
			}
			//$spec = Invspec::where("inv_is","=",$cats->inv_is)->where("name_spec","=",$cats->name_spec)->first();
			//print_r($spec);
			//dd($spec);
			//$spec->inv_is 		= $cats->id_inv;
			//$spec->spec_is 		= $cats->id_spec;
			//$spec->name_spec 	= $cats->name_spec;
			//$spec->value_is 	= $data->value_is;
			//$spec->save();
			//}
			//if($i==2){
				//die();
			//}
		$i++;
		}
		echo "Sukses : ".$i;
    }
	public function injectdata()
    {  
		dd("A");
		$i=0;
        $category = DB::table("tmp_inv_spec")->get();
	
		
		foreach($category as $cats){		
			if($cats->id_spec != null){
			
			$x = DB::table('inventory_specification_ec')
				->where("inv_is","=",$cats->inv_is)
				->where("name_spec","=",$cats->name_spec)->get()->first();
				if(count($x) >= 1) {
					DB::table('inventory_specification_ec')
						->where("inv_is","=",$cats->inv_is)
						->where("name_spec","=",$cats->name_spec)
						->where("spec_is","!=",$cats->id_spec)
						->update(['spec_is' => $cats->id_spec]);
				}else{
					DB::table('inventory_specification_ec')->insert(
						['inv_is' =>$cats->inv_is, "name_spec" => $cats->name_spec,"spec_is"=>$cats->id_spec,"value_is"=>$cats->value_is]
					);	
				}
			}
	
		$i++;
		}
		echo "Sukses : ".$i;
    }


}