<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paket extends Model
{
    protected $table = 'packet_spesial_ec';
	
	public function details(){
        return $this->hasMany(Paketdetail::class,'pkt_pktd','id_pkt');
    }
}
