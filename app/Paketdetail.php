<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Paketdetail extends Model
{
    protected $table = 'packet_detail_ec';
	
	public function variant(){
        return $this->hasMany(Variants::class,'var_pktd','id_var');
    }
	
	public function parents(){
        return $this->belongsTo(Discount::class,'pkt_pktd','id_pkt')
    }
}
