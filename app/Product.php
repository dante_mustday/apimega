<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    protected $table = 'inventory_ec';
	
	public function variants(){
        return $this->hasMany(Variants::class,'inv_var','id_inv')->where('inventory_ec.status_inv', '1')->where('inventory_ec.removed_inv',0);
    }
	
	public function related(){
        return $this->hasMany(Relatedproducts::class,'inv_ir','id_inv');
    }
}
