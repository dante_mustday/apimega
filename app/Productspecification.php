<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Productspecification extends Model
{
    protected $table = 'inventory_specification_ec_copy';
	
	public $timestamps = false;
	
	public function lists(){
        return $this->hasMany(Specification::class,'id_spec','spec_is');
    }
	
	public function variants(){
        return $this->belongsTo(Variants::class,'inv_is','inv_var');
    }
}
