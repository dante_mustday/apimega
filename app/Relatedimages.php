<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relatedimages extends Model
{
    protected $table = 'related_img_ec';
	
	public function collection(){
        return $this->hasMany(Variantimages::class,'id_img','img_rel');
    }
	
	public function variant(){
        return $this->belongsTo(Variants::class,'var_rel','id_var');
    }
}
