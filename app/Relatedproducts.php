<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relatedproducts extends Model
{
    protected $table = 'inventory_related_ec';

	public function product(){
        return $this->belongsTo(Product::class,'inv_ir','id_inv');
    }
	
	public function variant(){
        return $this->belongsTo(Variant::class,'inv_ir','inv_var');
    }
	
	public function products(){
        return $this->hasMany(Relatedproducts::class,'rel_ir','id_inv');
    }
}
