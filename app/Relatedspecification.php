<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Relatedspecification extends Model
{
    protected $table = 'inventory_specification_ec';
	
	public function lists(){
        return $this->hasMany(Specification::class,'id_spec','spec_is');
    }
	
	public function variants(){
        return $this->belongsTo(Variants::class,'inv_is','inv_var');
    }
}
