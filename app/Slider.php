<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $table = 'slider_ec';

    public function actived(){
        return $this->where('removed_slider',0)->where('status_slider',1);
    }
}
