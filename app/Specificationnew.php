<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Specificationnew extends Model
{
    protected $table = 'specification_ec_new';
	
	public $timestamps = false;
	
	public function products(){
        return $this->hasMany(Relatedspecification::class,'spec_is','id_spec');
    }
	
	public function spec(){
        return $this->belongsTo(Relatedspecification::class,'spec_is','id_spec');
    }
	
	public function detail(){
        return $this->where('status_spec', '1')
		->where('removed_spec',0);
    }
}
