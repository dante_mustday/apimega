<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subcategory extends Model
{
    protected $table = 'subcategory_ec';

    public function actived(){
        return $this->where('removed_scat',0)->where('status_scat',1);
    }
	
	public function category(){
        return $this->belongsTo(Category::class,'parent_scat','id_cat');
    }
	
	public function subsubcategory(){
        return $this->hasMany(Subsubcategory::class,'parent_sscat','id_scat')->where('removed_sscat',0)->where('status_sscat',1);
    }
	
}
