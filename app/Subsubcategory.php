<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Subsubcategory extends Model
{
    protected $table = 'subsubcategory_ec';

    public function actived(){
        return $this->where('removed_sscat',0)->where('status_sscat',1);
    }
    
	
}
