<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    protected $table = 'transaction_ec';
	public $timestamps = false;
	protected $primaryKey = 'id_trans';
	public function details(){
        return $this->hasMany(Transactiondetail::class,'trans_dt','id_trans');
    }

}
