<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Transactiondetail extends Model
{
    protected $table = 'transaction_detail_ec';
	public $timestamps = false;
	public function parents(){
        return $this->belongsTo(Transaction::class,'trans_dt','id_trans');
    }

}
