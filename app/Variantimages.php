<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variantimages extends Model
{
    protected $table = 'image_ec';
	
	public function related(){
        return $this->belongsTo(Relatedimages::class,'img_rel','id_img');
    }
}
