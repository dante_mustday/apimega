<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Variants extends Model
{
    protected $table = 'variant_ec';
	
	public function product(){
        return $this->belongsTo(Product::class,'inv_var','id_inv');
    }
	
	public function related(){
        return $this->hasMany(Relatedimages::class,'var_rel','id_var');
    }
	
	
	public function images(){
        return $this->hasMany(Relatedimages::class,'var_rel','id_var');
    }
	
	public function discount(){
        return $this->belongsTo(Discountdetail::class,'id_var','var_dd');
    }
	
	public function specification(){
        return $this->hasMany(Relatedspecification::class,'inv_is','inv_var');
    }
}
