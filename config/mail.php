<?php

return [
    'driver' =>  'smtp',
    'host' =>  'mail.megakamera.co.id',//'smtp.gmail.com',
    'port' => 25,//587,
    'from' => [
        'address' => 'do-not-reply@megakamera.co.id',//'iagus048@gmail.com',
        'name' => 'megakamera.com',//'Indara Tes',
    ],
    'stream' => [
		'ssl' => [
		    'allow_self_signed' => true,
		    'verify_peer' => false,
		    'verify_peer_name' => false,
		],
	],
    'encryption' => 'tls',
    'username' => 'do-not-reply@megakamera.co.id',//'iagus048@gmail.com',
    'password' => 'd0-n0t-r3ply!',
    'sendmail' => '/usr/sbin/sendmail -bs',
    'markdown' => [
        'theme' => 'default',

        'paths' => [
            resource_path('views/vendor/mail'),
        ],
    ],

];
