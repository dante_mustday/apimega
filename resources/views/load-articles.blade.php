<?php
$i= 0;
foreach($articles as $z){
if($i<4){
	$title=$z['title'];
	$url=$z['URL'];
	$excerpt=substr(strip_tags($z['excerpt']),0,200);
	$img=$z['featured_image'];
	$date=$z['date'];
?>
	<div class="col-xs-3 banner top">
		<div class="col-xs-12" style="background:white;padding:10px;text-align:justify">
			<img src="<?php echo $img;?>" class="img-responsive">
			<p class="title-blog"><?php echo substr(strip_tags($title),0,75);?>...</p>
			<span class="date"><i class="fa fa-calendar"></i> <?php echo date_format(date_create($date),"d M Y");?></span>
			<!--<p style="text-align:justify"><?php echo $excerpt;?> .... <a href="<?php echo $url;?>" class="readmore">Read More</a></p>-->
		</div>
	</div>		
<?php $i++;} } ?>	