		@if(count($cat->product)>0)	
			<div class="container" style="border-top:2px solid maroon;padding:0px;margin-bottom: 30px;">
				<div style="background:white;float:left;width:100%">
					<div class="col-md-2" style="text-align:center">
						<h4 style="border-bottom:0.5px solid lightgrey;padding-bottom:15px">{{ $cat->label_cat }}</h4>
						<ul style="list-style-type: none;padding:0px;">
							<?php 
								$n = 1;
								$limit = 11;
								foreach ($cat->subcategory as $value2) {
								?>
								<li style="padding: 8px;font-size:13px">
									<a href="/products?category={{ $value2->link_scat }}">
										<span>&nbsp;<?php echo $value2->label_scat ?></span>  
									</a>
								</li>
								<?php }
								 
							?>
						</ul>
					</div>
					<div class="col-md-4" style="padding:0px;">
						<img src="backend/uploads/category_ecommerce/{{ $cat->banner_cat }}" class="img-responsive">
					</div>
					<div class="col-md-6">
						<div class="row">
						<?php foreach($cat->product as $value){?>
							<a style="padding-bottom: 20px;text-align: left;" href="/product/{{ $value->permalink_inv }}/{{ $value->id_var }}"  class="col-xs-4 col-sm-4 col-md-4 col-lg-4">
							<?php 
							$img = (isset($value->src_img)) ? $value->id_inv."/".$value->src_img : "";
							$img = 'backend/uploads/inventory_ecommerce/'.$img;
							$price = (!empty($value->price_var)) ? $value->price_var : 0;?>
										
								<!--<span class="label label-success tag">New</span>-->
								<img class="img-responsive lazy" src="<?php echo $img ?>" alt=""/>
								<div class="caption">
									<h5 class="title-product"><?php echo substr($value->name_inv,0,45); ?>
									<br/>
									<?php if($value->preorder_inv==0){ 
									if($value->stock_var>=1){ ?>
										<span  style="margin:0px 3px;font-size: 10px;color:green">
											IN STOCK
										</span>		
									<?php  }else{ ?>
											<span  style="margin:0px 3px;font-size: 10px;color:red">
												BY ORDER
											</span>
									<?php } } else if($value->preorder_inv==1){ ?>
											<span  style="margin:0px 3px;font-size: 10px;color:orange">
												Preorder
											</span>
									<?php }else{ ?>
										<span  style="margin:0px 3px;font-size: 10px;color:orange">
												Discontinue
											</span>
									<?php } ?>
									</h5>
									
									<h4 class="price-product">
										<?php 

									if($value->type_inv==1){?>
									Rp. <?php echo number_format($price);?>
									<?php }else{
									echo "<span style='color:red'>Call US</span>";
									}?>
									</h4>
								</div>
							</a>
						<?php }?>
						</div>
					</div>
				</div>
			</div>
		@endif