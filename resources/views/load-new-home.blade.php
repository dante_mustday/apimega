<?php 
	$i=1;
	foreach ($new as $value) { 
?>
	<a style="padding-bottom: 20px;text-align: left;" href="<?php echo $value['link']; ?>"  class="col-xs-4 col-sm-4 col-md-4 col-lg-2">
		<span class="label label-success tag"><?php echo $value['label'];?></span>
		<img class="img-responsive lazy" src="<?php echo $value['image'] ?>" alt=""/>
		<div class="caption"> 
			<h5 class="title-product"><?php echo substr($value['name'],0,45); ?>
			<br/>
				<?php echo $value['stock_label'];?>
			</h5>
			<h4 class="price-product">
				<?php echo $value['price_label']; ?>
			</h4>
		</div>
	</a>
<?php 
	} 
?>