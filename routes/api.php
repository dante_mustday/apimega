<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});
*/

$router->group(['prefix' => 'kase'], function($app)
{	

	$app->post('register_app_id','Api\ExampleController@register_app_id');
	$app->get('blog_category','Api\ExampleController@blog_category');
	$app->get('blog','Api\ExampleController@blog');
	$app->get('blog/{slug}','Api\ExampleController@blogDetail');

	$app->get('ambasador','Api\ExampleController@ambasador');
	$app->get('ambasador/{slug}','Api\ExampleController@ambasadorDetail');


	$app->get('product','Api\ExampleController@product');
	$app->get('product/{slug}','Api\ExampleController@productDetail');

	$app->get('dealer','Api\ExampleController@dealer');

	$app->get('video','Api\ExampleController@video');

	$app->get('promo','Api\ExampleController@promo');
	$app->get('promo/{slug}','Api\ExampleController@promoDetail');
	
});

Route::post('/v1/register_app_id', 'Api\SettingController@registerId');

Route::post('/v2/loginGoogle', 'Api\AuthController@loginGoogle');
Route::post('/v2/loginFacebook', 'Api\AuthController@loginFacebook');
Route::post('/v2/api-login', 'Api\AuthController@login');
Route::post('/v2/change-password', 'Api\AuthController@password');
Route::post('/v2/update-profile', 'Api\AuthController@profile');
Route::post('/v2/forgot-password', 'Api\AuthController@reset_password');
Route::post('/v2/register', 'Api\AuthController@register');


Route::get('/v2/slider', 'Api\SliderController@listswithpromo');
Route::get('/v2/banner/{position}', 'Api\BannerController@lists');
Route::get('/v2/brand', 'Api\BrandController@lists');
Route::post('/v2/brand-home', 'Api\BrandController@loadHome');
Route::get('/v2/tags', 'Api\BrandController@tags');

Route::get('/v2/categorybulk', 'Api\CategoryController@bulk'); 

Route::get('/v2/category', 'Api\CategoryController@lists');
Route::get('/v2/category-by-id/{id}', 'Api\CategoryController@categoryDetailWithSub');
Route::get('/v2/subcategory-by-id/{id}', 'Api\CategoryController@subcategoryDetailWithSub');
Route::get('/v2/subsubcategory-by-id/{id}', 'Api\CategoryController@subsubcategoryDetail');

Route::get('/v2/product', 'Api\ProductController@lists');
Route::get('/v2/product/promo', 'Api\ProductController@promo');
Route::get('/v2/product/hot', 'Api\ProductController@hot');
Route::get('/v2/product/selloff', 'Api\ProductController@selloff');
Route::get('/v2/product/new', 'Api\ProductController@news');
Route::get('/v2/product/detail/{id_var}', 'Api\ProductController@detail');

Route::get('/v2/new-product-home', 'Api\ProductController@homenew');
Route::get('/v2/promo-product-home', 'Api\ProductController@homepromo');

Route::get('/v2/articles', 'Api\WordpressController@index');


Route::get('/v2/paket-category', 'Api\PaketController@category');
Route::get('/v2/paket', 'Api\PaketController@lists');
Route::get('/v2/paket/detail/{id_pkt}', 'Api\PaketController@detail');


Route::get('/v2/general/city', 'Api\SettingController@allCity');
Route::get('/v2/general/setting', 'Api\SettingController@settingCompany');
Route::post('/v2/general/search-city', 'Api\SettingController@searchCity');
Route::get('/v2/general/district/{id}', 'Api\SettingController@districtByCity');

Route::get('/v2/transaction/{customer}/{status}', 'Api\TransactionController@lists');
Route::get('/v2/transaction/submit', 'Api\TransactionController@submit');
Route::get('/v2/transaction-detail/{invoice}', 'Api\TransactionController@detail');

Route::get('/v2/product-bulk', 'Api\ProductController@index');

Route::post('/v2/load-home', 'Api\CategoryController@homeload');
Route::post('/v2/load-home2', 'Api\CategoryController@homeload2');


Route::get('/v2/event', 'Api\EventController@lists');
Route::get('/v2/event/{id}', 'Api\EventController@detail');
Route::post('/v2/requestItem', 'Api\EventController@requestItem');
Route::get('/v2/about', 'Api\EventController@about');

Route::get('/v2/promo', 'Api\PromoController@lists');
Route::get('/v2/promo-list', 'Api\PromoController@listsProduct');
Route::get('/v2/reward', 'Api\RewardController@lists');

Route::post('/v2/checkout', 'Api\CheckoutController@submit');
Route::get('/v2/sendEmail/{invoicenumber}', 'Api\CheckoutController@sendEmail');
Route::get('/v2/getVoucher/{code}', 'Api\CheckoutController@getVoucher');
Route::get('/v2/getBank', 'Api\TransactionController@bank');
Route::post('/v2/paymentConfirmation', 'Api\TransactionController@paymentConfirmation');

Route::post('/v2/getOnWishList', 'Api\TransactionController@getOnWishList');
Route::post('/v2/setOnWishlist', 'Api\TransactionController@setOnWishlist');
Route::get('/v2/wishlist/{id_cus}', 'Api\TransactionController@wishlist');
//Route::post('/v2/paymentConfirmation', 'Api\TransactionController@paymentConfirmation');
//Route::post('/v2/paymentConfirmation', 'Api\TransactionController@paymentConfirmation');
