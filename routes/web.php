<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});  

Route::get('/render', 'Web\CoreController@index');
Route::get('/render-data', 'Web\CoreController@renderdata');
Route::get('/inject-data', 'Web\CoreController@injectdata');
